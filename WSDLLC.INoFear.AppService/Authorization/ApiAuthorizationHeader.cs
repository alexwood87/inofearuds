﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WSDLLC.INoFear.AppService.Authorization
{
    public class ApiAuthorizationHeader : AuthorizeAttribute
    {
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            var username = filterContext.HttpContext.Request["Header_Username"];
            var password = filterContext.HttpContext.Request["Header_Password"];
            if(password != "3228ED3F-56CA-4DA1-9E4F-9027BEC18427" || username != "UDSUser")
            {
                throw new UnauthorizedAccessException("You are not authorized to use this api");
            }
        }
    }
}