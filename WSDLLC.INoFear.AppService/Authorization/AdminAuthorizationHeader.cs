﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WSDLLC.INoFear.Domain.Models;

namespace WSDLLC.INoFear.AppService.Authorization
{
    public class AdminAuthorizationHeader : AuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            var model = (AdminstratorModel)httpContext.Session["User"];
            if (model == null)
            {
                httpContext.Response.Redirect("~/Home/Login");
                return false;
            }
            if(httpContext.Request.Path.Contains("/AllAdmins") || httpContext.Request.Path.Contains("/EditAdmin") ||
                httpContext.Request.Path.Contains("/AddAdmin")|| httpContext.Request.Path.Contains("/DeleteAdmin"))
            {
                if(model.AuthLevel != AuthenticationLevel.SUPERADMIN)
                {
                    httpContext.Response.StatusCode = 401;
                    httpContext.Response.Redirect("/Admin/Index");
                    return false;
                }
            }
            return true;
        }
    }
}