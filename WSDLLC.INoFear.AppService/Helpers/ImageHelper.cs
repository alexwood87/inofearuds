﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WSDLLC.INoFear.AppService.Helpers
{
    public class ImageHelper
    {
        public static byte[] ConvertBase64Image(string image)
        {
            return
                Convert.FromBase64String(image);
        }   
    }
}