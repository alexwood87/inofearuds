﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WSDLLC.INoFear.AppService.Helpers
{
    public static class HtmlExtensions
    {
        public static List<SelectListItem> AuthLevelDropDown(int level)
        {
            return
                new List<SelectListItem>
                {
                    new SelectListItem
                    {
                        Text = "SUPER ADMIN",
                        Value = "0",
                        Selected = level == 0
                    },
                    new SelectListItem
                    {
                        Text = "ADMIN",
                        Value = "1",
                        Selected = level == 1
                    },
                    new SelectListItem
                    {
                        Text = "MODERATOR",
                        Value = "2",
                        Selected = level == 2
                    }
                };
        }
    }
}