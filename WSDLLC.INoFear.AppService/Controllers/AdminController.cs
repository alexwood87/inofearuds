﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WSDLLC.INoFear.AppService.Authorization;
using WSDLLC.INoFear.BusinessLogic;
using WSDLLC.INoFear.Core.Contracts;
using WSDLLC.INoFear.Domain.Models;

namespace WSDLLC.INoFear.AppService.Controllers
{
    [AdminAuthorizationHeader]
    public class AdminController : Controller
    {
        private readonly IAdminBusinessLogic _logic;
        // GET: Admin
        public AdminController()
        {
            _logic = new AdminBusinessLogic(ConfigurationManager.ConnectionStrings["ConnString"].ConnectionString);
        }
        public ActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public ActionResult AllAdmins()
        {
            var model = (AdminstratorModel)Session["User"];
            return
                View(_logic.AllAdministrators(model.AuthLevel));
        }
        [HttpGet]
        public ActionResult CreateAdmin()
        {
            return 
                View();
        }
        [HttpPost]
        public ActionResult CreateAdmin(AdminstratorModel model)
        {
            var model2 = (AdminstratorModel)Session["User"];
            try
            {
                
                _logic.AddOrUpdateAdmin(model, model2.AuthLevel);
                return
                    RedirectToAction("AllAdmins");
            }
            catch(Exception ex)
            {
                return
                    Redirect("~/Admin/Index");
            }
        }
        
        public ActionResult DeleteAdmin(int id)
        {
            var model = (AdminstratorModel)Session["User"];
            _logic.DeleteAdmin(id, model.AuthLevel);
            return
                RedirectToAction("AllAdmins");
        }
        [HttpPost]
        public ActionResult EditAdmin(AdminstratorModel model)
        {
            var model2 = (AdminstratorModel)Session["User"];
            try
            {
                _logic.AddOrUpdateAdmin(model, model2.AuthLevel);
                return
                    RedirectToAction("AllAdmins");
            }
            catch (Exception ex)
            {
                return
                    Redirect("~/Admin/Index");
            }
        }

        public ActionResult CertifyProtector(long protectorId)
        {
            try
            {
                _logic.CertifyProtector(protectorId);
                return
                    Json(new { Success = true }, JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                return
                    Json(new { Success = false, Error = ex.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult ChangeProtectorType(string protectorType, long protectorId)
        {
            try
            {
                _logic.ChangeProtectorType(protectorId, protectorType);
                return
                    Json(new { Success = true }, JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                return
                    Json(new { Success = false, Error = ex.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult UnCertifyProtector(long protectorId)
        {
            try
            {
                _logic.UnCertifyProtector(protectorId);
                return
                    Json(new { Success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return
                    Json(new { Success = false, Error = ex.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult SearchProtectors(bool certified, int pageIndex)
        {
            try
            {
                int total =0;
                var protectors = _logic.SearchProtectors(certified, pageIndex, out total);
                var jres = Json(new { Data = protectors, Success = true, Total = total }, JsonRequestBehavior.AllowGet);
                jres.MaxJsonLength = int.MaxValue;
                jres.RecursionLimit = 100;
                return
                    jres;
            }
            catch(Exception ex)
            {
                return
                    Json(new { Success = false, FriendlyMessage = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        
    }
}