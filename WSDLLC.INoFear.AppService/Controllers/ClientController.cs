﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WSDLLC.INoFear.BusinessLogic;
using WSDLLC.INoFear.Core.Contracts;
using WSDLLC.INoFear.Domain.Models;

namespace WSDLLC.INoFear.AppService.Controllers
{
    public class ClientController : Controller
    {
        private readonly IAppBusinessLogic _logic;
        public ClientController()
        {
            _logic = new AppBusinessLogic(ConfigurationManager.AppSettings["ConnString"]);
        }
        // GET: Client
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Index(string phoneNumber, string pin)
        {
            var p = _logic.SearchProtectees(new PersonDetailsSearch
            {
                PhoneNumber = phoneNumber,
                PageSize = 1
            }).SingleOrDefault(x => x.Password == pin);
            if (p == null)
            {
                var p2 = _logic.SearchProtectors(new PersonDetailsSearch
                {
                    PageSize = 1,
                    PhoneNumber = phoneNumber
                }).SingleOrDefault(x => x.Password == pin);
                if(p2 == null)
                {
                    return
                        View();
                }
                Session["Client"] = p2;
                return
                    RedirectToAction("SearchTransactionsProtectors");
            }
            Session["Client"] = p;
            return
                RedirectToAction("SearchTransactionsProtectees");
        }
        [HttpGet]
        public ActionResult ProtectorDetails(long id)
        {
            var c = Session["Client"] as Protector;
            if(c == null)
            {
                return
                    Redirect("~/Client/Index");
            }
            return
                View(_logic.SearchTransactions(new TransactionSearch
                {
                    PageSize =1,
                    Id = id
                }).Single());
        }
        [HttpGet]
        public ActionResult ProtecteeDetails(long id)
        {
            var cl = Session["Client"] as Protectee;

            if (cl == null)
            {
                return
                    Redirect("~/Client/Index");
            }
            return
                View(_logic.SearchTransactions(new TransactionSearch
                {
                    PageSize =1,
                    Id = id
                }).Single());
        }
        [HttpGet]
        public ActionResult SearchTransactionsProtectees()
        {
            var cl = Session["Client"] as Protectee;

            if (cl == null)
            {
                return
                    Redirect("~/Client/Index");
            }
            return
                View();
        }
        [HttpGet]
        public ActionResult SearchTransactionsProtectors()
        {
            var c = Session["Client"] as Protector;
            if (c == null)
            {
                return
                    Redirect("~/Client/Index");
            }

            return
                View();
        }
        [HttpPost]
        public ActionResult SearchTransactionsProtectees(DateTime start)
        {
            var cl = Session["Client"] as Protectee;
            
            if (cl == null)
            {
                return
                    Redirect("~/Client/Index");
            }
            var c = _logic.SearchTransactions(new TransactionSearch
            {
                DateCreated = start,
                PageSize = int.MaxValue,
                ProtecteeId = cl.ProtecteeId
            }).Where(x => x.Protector != null && !x.WasCancelled);
            return
                View(c);
        }
        [HttpPost]
        public ActionResult SearchTransactionProtectors(DateTime start)
        {
            var cl = Session["Client"] as Protector;
          
            if (cl == null)
            {
                return
                    Redirect("~/Client/Index");
            }
            var c = _logic.SearchTransactions(new TransactionSearch
            {
                ProtectorId = cl.ProtectorId,
                PageSize = int.MaxValue,
                DateCreated = start
            });
            return
                View(c);
        }
    }
}