﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WSDLLC.INoFear.BusinessLogic;
using WSDLLC.INoFear.Core.Contracts;
using WSDLLC.INoFear.Domain.Models;
using WSDLLC.INoFear.AppService.Authorization;
namespace WSDLLC.INoFear.AppService.Controllers
{
    [ApiAuthorizationHeader]
    public class INoFearApiController : Controller
    {
        private readonly IAppBusinessLogic _logic;
        private readonly IAdminBusinessLogic _adminLogic;
        public INoFearApiController()
        {
            _adminLogic = new AdminBusinessLogic(ConfigurationManager.ConnectionStrings["ConnString"].ConnectionString);
            _logic = new AppBusinessLogic(ConfigurationManager.ConnectionStrings["ConnString"].ConnectionString);
        }

        public JsonResult RegisterProtectee(Protectee p, string Picture64)
        {
            try
            {
                if (Picture64 != null)
                {
                    p.Picture = Helpers.ImageHelper.ConvertBase64Image(Picture64);
                }
                p.ProtecteeId = _logic.RegisterProtectee(p, null);
                p.Picture = null;
                try
                {
                    _adminLogic.SendStatusEmail(null, p.ProtecteeId, "<html><body>Welcome " + p.FirstName + " " + p.LastName + " To UBS <br/>- Your Phone Number and pin are your login.<br/> Please visit:<a href='http://www.UBS.com/Welcome/FAQ?phoneNumber=" + p.PhoneNumber + "'>The Site</a> For Frequently Asked Questions!</body></html>", "Welcome to UBS");
                }
                catch (Exception ex)
                {

                }
                var jres = Json(new { Success = true, Data = p }, JsonRequestBehavior.AllowGet);
                jres.MaxJsonLength = int.MaxValue;
                jres.RecursionLimit = 100;
                return jres;
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Data = new Protectee
                    {
                        ProtecteeId = -1
                    },
                    Error = ex.ToString(),
                    Success = false
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult RegisterOrUpdateProtector(Protector p, string Picture64, Payment pay)
        {
            try
            {
                if (Picture64 != null)
                {
                    p.Picture = Helpers.ImageHelper.ConvertBase64Image(Picture64);
                }
                p.ProtectorId = _logic.RegisterProtector(p, pay);
                try
                {
                    _adminLogic.SendStatusEmail(p.ProtectorId, null, "<html><body>Welcome " + p.FirstName + " " + p.LastName + " To UDS <br/>- Your Phone Number and pin are your login.<br/> Please visit:<a href='http://www.UDS.com/CeritfyMe?phoneNumber=" + p.PhoneNumber + "'>The Site</a> To Become Certified!</body></html>", "Welcome to UBS");
                }
                catch (Exception ex)
                {

                }
                var jres =
                     Json(new { Data = p, Success = true }, JsonRequestBehavior.AllowGet);
                jres.MaxJsonLength = int.MaxValue;
                jres.RecursionLimit = 100;
                return jres;
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    Data =
                    new Protector
                    {
                        ProtectorId = -1
                    },
                    Error = ex.Message + ex.InnerException?.ToString()
                }, JsonRequestBehavior.AllowGet);

            }
        }
        public  JsonResult AddChat(string message, string protecteePhoneNumber, string protectorPhoneNumber, bool isFromProtector)
        {
            try
            {
                if(string.IsNullOrEmpty(message))
                {
                    return
                        Json(new {Success = false }, JsonRequestBehavior.AllowGet);
                }
                _logic.AddChat(message, protectorPhoneNumber, protecteePhoneNumber, isFromProtector);
                return Json(new { Success = true, Data = "AddChat" }, JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                return
                    Json(new { Error = ex.Message + " " + ex.InnerException ?? "", Success = false }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetLatestChat(string protecteePhoneNumber, string protectorPhoneNumber, bool isFromProtector)
        {
            try
            {
                return
                    Json(new { Data = _logic.GetMostCurrentChat(protectorPhoneNumber, protecteePhoneNumber, isFromProtector), Success = true  }, JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                return
                    Json(new { Success = false, Error = ex.Message + " " + ex.InnerException??"" + ex.InnerException?.InnerException }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult SearchCoordinates(Search search)
        {
            try
            {
                var jres = Json(new
                {
                    Success = true,
                    Data = _logic.SearchCoordinates(search)
                }, JsonRequestBehavior.AllowGet);
                jres.MaxJsonLength = int.MaxValue;
                jres.RecursionLimit = 100;
                return jres;
            }
            catch (Exception ex)
            {
                return
                    Json(new { Success = false, Error = ex.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult AddOrUpdateCoordinate(Coordinate coord, string payeePhoneNumber, string protectorPhoneNumber)
        {
            try
            {
                if (!string.IsNullOrEmpty(payeePhoneNumber))
                {
                    coord.Protectee = _logic.SearchProtectees(new PersonDetailsSearch
                    {
                        PhoneNumber = payeePhoneNumber,
                        PageSize = 1
                    }).Single();
                }
                if (!string.IsNullOrEmpty(protectorPhoneNumber))
                {
                    coord.Protector = _logic.SearchProtectors(new PersonDetailsSearch
                    {
                        PageSize = 1,
                        PhoneNumber = protectorPhoneNumber
                    }).Single();
                }
                coord.CoordinateId = _logic.AddUpdateCoordinates(coord);
                var jres = Json(new { Data = coord, Success = true }, JsonRequestBehavior.AllowGet);
                jres.MaxJsonLength = int.MaxValue;
                jres.RecursionLimit = 100;
                return jres;
            }
            catch (Exception ex)
            {
                return
                    Json(new { Success = false, Error = ex.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult ValidateProtectee(string phoneNumber, string password)
        {
            try
            {
                if (string.IsNullOrEmpty(phoneNumber))
                {
                    return
                        Json(new { Success = false, Error = "Phone Number is null" }, JsonRequestBehavior.AllowGet);
                }
                var p = _logic.SearchProtectees(new PersonDetailsSearch
                {
                    PhoneNumber = phoneNumber,
                    PageSize = 1
                }).SingleOrDefault(x => x.Password == password);
                var jres =
                    p == null ? Json(new { Success = false }, JsonRequestBehavior.AllowGet) : Json(new { Data = p, Success = true }, JsonRequestBehavior.AllowGet);
                jres.MaxJsonLength = int.MaxValue;
                jres.RecursionLimit = 100;
                return jres;
            }
            catch (Exception ex)
            {
                return
                    Json(new { Error = ex.ToString(), Success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult ValidateProtector(string phoneNumber, string password)
        {
            try
            {
                if (string.IsNullOrEmpty(phoneNumber))
                {
                    return
                        Json(new
                        {
                            Success = false,
                            Error = "Phone number is null"
                        }, JsonRequestBehavior.AllowGet);
                }
                var p = _logic.SearchProtectors(new PersonDetailsSearch
                {
                    PageSize = 1,
                    PhoneNumber = phoneNumber
                }).SingleOrDefault(x => x.Password == password);
                var jres =
                    p == null ? Json(new { Success = false }, JsonRequestBehavior.AllowGet) : Json(new { Data = p, Success = true }, JsonRequestBehavior.AllowGet);
                jres.MaxJsonLength = int.MaxValue;
                jres.RecursionLimit = 100;
                return jres;
            }
            catch (Exception ex)
            {
                return
                    Json(new { Error = ex.ToString(), Success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult SearchProtectors(PersonDetailsSearch search)
        {
            try
            {
                var jres = Json(new
                {
                    Data =
                        _logic.SearchProtectors(search),
                    Success = true
                }, JsonRequestBehavior.AllowGet);
                jres.MaxJsonLength = int.MaxValue;
                jres.RecursionLimit = 100;
                return jres;
            }
            catch (Exception ex)
            {
                return
                    Json(new { Error = ex.ToString(), Success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult SearchProtectees(PersonDetailsSearch search)
        {
            try
            {
                var jres = Json(new
                {
                    Data =
                    _logic.SearchProtectees(search),
                    Success = true
                }, JsonRequestBehavior.AllowGet);
                jres.MaxJsonLength = int.MaxValue;
                jres.RecursionLimit = 100;
                return jres;
            }
            catch (Exception ex)
            {
                return
                    Json(new { Error = ex.ToString(), Success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult SearchTransactions(TransactionSearch search)
        {
            try
            {
                search.PageSize = 50;
                var jres = Json(new
                {
                    Success = true,
                    Data =
                    _logic.SearchTransactions(search)
                }, JsonRequestBehavior.AllowGet);
                jres.MaxJsonLength = int.MaxValue;
                jres.RecursionLimit = 100;
                return jres;
            }
            catch (Exception ex)
            {
                return
                    Json(new { Error = ex.ToString(), Success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult SearchPayments(string phoneNumber)
        {
            try
            {
                var payee = _logic.SearchProtectees(new PersonDetailsSearch
                {
                    PhoneNumber = phoneNumber,
                    PageSize = 1
                }).SingleOrDefault();
                if (payee == null)
                {
                    var pro = _logic.SearchProtectors(new PersonDetailsSearch
                    {
                        PhoneNumber = phoneNumber,
                        PageSize = 1
                    }).SingleOrDefault();
                    if (pro == null)
                    {
                        return
                            Json(new { Error = "Invalid Phone Number", Success = false }, JsonRequestBehavior.AllowGet);
                    }
                    var pays = _logic.SearchPayments(new Search
                    {
                        ProtectorId = pro.ProtectorId,
                        PageSize = int.MaxValue
                    }).Where(x => !x.IsInvalid).ToList();
                    var jres2 =
                        Json(new { Success = true, Data = pays }, JsonRequestBehavior.AllowGet);
                    jres2.MaxJsonLength = int.MaxValue;
                    jres2.RecursionLimit = 100;
                    return jres2;
                }
                var jres = Json(new
                {
                    Data = _logic.SearchPayments(new Search
                    {
                        ProtecteeId = payee.ProtecteeId,
                        PageSize = int.MaxValue
                    }).Where(x => !x.IsInvalid).ToList(),
                    Success = true
                }, JsonRequestBehavior.AllowGet);
                jres.MaxJsonLength = int.MaxValue;
                jres.RecursionLimit = 100;
                return jres;
            }
            catch (Exception ex)
            {
                return
                    Json(new { Error = ex.ToString(), Success = false }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult DeletePayment(Payment p, string phoneNumber)
        {
            try
            {
                var wasSuccess = _logic.DeletePayment(phoneNumber, p);
                return
                    Json(new { Success = wasSuccess }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return
                    Json(new { Success = false, Error = ex.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult RegisterPayment(Payment p, string phoneNumber)
        {
            try
            {
                var payId = _logic.RegisterPayment(p, phoneNumber);
                p.PaymentId = payId;

                var jres = p == null
                    || p.IsInvalid || payId <= 0 ?
                    Json(new { Success = false, Error = "Failed To Register Payment" }, JsonRequestBehavior.AllowGet)
                    : Json(new { Data = p, Success = true }, JsonRequestBehavior.AllowGet);
                jres.MaxJsonLength = int.MaxValue;
                jres.RecursionLimit = 100;
                return jres;
            }
            catch (Exception ex)
            {
                return
                    Json(new { Success = false, Error = ex.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult SearchCoordinate(string ProtecteePhoneNumber, string ProtectorPhoneNumber, decimal latitude, decimal longitude)
        {
            try
            {
                long? proId = null, preId = null;
                if (!string.IsNullOrEmpty(ProtecteePhoneNumber))
                {
                    preId = _logic.SearchProtectees(new PersonDetailsSearch
                    {
                        PageSize = 1,
                        PhoneNumber = ProtecteePhoneNumber
                    }).Single().ProtecteeId;
                }
                if (!string.IsNullOrEmpty(ProtectorPhoneNumber))
                {
                    proId = _logic.SearchProtectors(new PersonDetailsSearch
                    {
                        PhoneNumber = ProtectorPhoneNumber,
                        PageSize = 1
                    }).Single().ProtectorId;
                }
                var coords = _logic.SearchCoordinates(new Search
                {
                    ProtecteeId = preId,
                    ProtectorId = proId,
                    PageSize = int.MaxValue
                });
                var jres =
                    Json(new { Success = true, Data = coords.OrderByDescending(x => x.DateCreated).FirstOrDefault() }, JsonRequestBehavior.AllowGet);
                jres.MaxJsonLength = int.MaxValue;
                jres.RecursionLimit = 100;
                return jres;
            }
            catch (Exception ex)
            {
                return
                    Json(new { Success = false, Error = ex.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult SearchPayeeTransactionsNearMe(string phoneNumber, decimal lat, decimal lon)
        {
            try
            {
                var pro = _logic.SearchProtectors(new PersonDetailsSearch
                {
                    PhoneNumber = phoneNumber,
                    PageSize = 1
                }).Single();
                var trans = _logic.SearchTransactions(new TransactionSearch
                {
                    Latitude = lat,
                    Longitude = lon,
                    PageSize = 25,
                    DateCreated = DateTime.UtcNow.AddHours(-1),
                    WasCancelled = false,
                    ProtectorId = null,
                    ProtectorType = pro.ProtectorType                  
                }).ToArray();

                var jres =
                    Json(new { Success = true, Data = trans }, JsonRequestBehavior.AllowGet);
                jres.MaxJsonLength = int.MaxValue;
                jres.RecursionLimit = 100;
                return jres;
            }
            catch (Exception ex)
            {
                return
                    Json(new { Success = false, Error = ex.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult RateProtectee(string protectorPhoneNumber, string payeePhoneNumber, int rating)
        {
            try
            {
                var payee = _logic.SearchProtectees(new PersonDetailsSearch
                {
                    PageSize = 1,
                    PhoneNumber = payeePhoneNumber
                }).Single();
                var protector = _logic.SearchProtectors(new PersonDetailsSearch
                {
                    PageSize = 1,
                    PhoneNumber = protectorPhoneNumber
                }).Single();
                var ratingModel = new Rating
                {
                    IsForProtector = false,
                    Protectee = payee,
                    Protector = protector,
                    RatingAmount = rating
                };
                _logic.Rate(ratingModel);
                var jres =
                    Json(new { Success = true, Data = ratingModel }, JsonRequestBehavior.AllowGet);
                jres.MaxJsonLength = int.MaxValue;
                jres.RecursionLimit = 100;
                return jres;
            }
            catch (Exception ex)
            {
                return
                    Json(new { Success = false, Error = ex.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }
        
        public JsonResult RateProtector(string protectorPhoneNumber, string payeePhoneNumber, int rating)
        {
            try
            {
                var payee = _logic.SearchProtectees(new PersonDetailsSearch
                {
                    PageSize = 1,
                    PhoneNumber = payeePhoneNumber
                }).Single();
                var protector = _logic.SearchProtectors(new PersonDetailsSearch
                {
                    PageSize = 1,
                    PhoneNumber = protectorPhoneNumber
                }).Single();
                var ratingModel = new Rating
                {
                    IsForProtector = true,
                    Protectee = payee,
                    Protector = protector,
                    RatingAmount = rating
                };
                _logic.Rate(ratingModel);
                var jres =
                    Json(new { Success = true, Data = ratingModel }, JsonRequestBehavior.AllowGet);
                jres.MaxJsonLength = int.MaxValue;
                jres.RecursionLimit = 100;
                return jres;
            }
            catch(Exception ex)
            {
                return
                    Json(new { Success = false, Error = ex.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult AcceptMission(string protectorPhoneNumber, long transactionId)
        {
            try
            {
                var trans = _logic.SearchTransactions(new TransactionSearch
                {
                    Id = transactionId,
                    PageSize = 1
                }).Single();
                var protector = _logic.SearchProtectors(new PersonDetailsSearch
                {
                    PhoneNumber = protectorPhoneNumber,
                    PageSize =1
                }).Single();
                trans.Protector = protector;
                var pays = _logic.SearchPayments(new Search
                {
                    ProtecteeId = trans.Protectee.ProtecteeId,
                    PageSize = int.MaxValue
                }).FirstOrDefault(x => !x.IsInvalid);
                if (pays == null)
                {
                    return
                        Json(new { Success = false, Error = "No Payment Found" }, JsonRequestBehavior.AllowGet);
                }
                trans.PaymentId = pays.PaymentId;
                _logic.MakeTransaction(trans);
                var jres =
                    Json(new { Success = true, Data = trans }, JsonRequestBehavior.AllowGet);
                jres.MaxJsonLength = int.MaxValue;
                jres.RecursionLimit = 100;
                return jres;
            }
            catch (Exception ex)
            {
                return
                    Json(new { Success = false, Error = ex.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult RequestTransaction(string phoneNumber, decimal lat, decimal lon, string type)
        {
            try
            {
                var cust = _logic.SearchProtectees(new PersonDetailsSearch
                {
                    PageSize = 1,
                    PhoneNumber = phoneNumber
                }).SingleOrDefault();
                if(cust == null)
                {
                    return
                        Json(new { Success = false, Error = "Invalid Phone Number" }, JsonRequestBehavior.AllowGet);
                }
                var pays = _logic.SearchPayments(new Search
                {
                    ProtecteeId = cust.ProtecteeId,
                    PageSize = int.MaxValue
                }).FirstOrDefault(x => !x.IsInvalid);
                if(pays == null)
                {
                    return
                        Json(new { Success = false, Error = "You have no valid credit cards!"}, JsonRequestBehavior.AllowGet);
                }
                lat = decimal.Parse(lat.ToString().Substring(0, lat.ToString().Length>7 ? 7 : lat.ToString().Length));
                lon = decimal.Parse(lon.ToString().Substring(0, lon.ToString().Length >7 ? 7 : lon.ToString().Length));
                var coords = _logic.AddUpdateCoordinates(new Coordinate
                {
                    DateCreated = DateTime.UtcNow,
                    Latitude = (decimal)(float)lat,
                    Longitude = (decimal)(float)lon,
                    Protectee = _logic.SearchProtectees(new PersonDetailsSearch
                    {
                        PhoneNumber = phoneNumber,
                        PageSize = 1
                    }).Single()
                });
                var coor = _logic.SearchCoordinates(new Search
                {
                    Id = coords,
                    PageSize = 1
                }).Single();
                var l= _logic.MakeTransaction(new ProtectionTransaction
                {
                    Protectee = cust,
                    AmountCharged =0,
                    DateCreated = DateTime.UtcNow,
                    StartCoordinate = coor,
                    EndDate = DateTime.UtcNow.AddHours(5),
                    ProtectionType = type,
                    PaymentId = pays.PaymentId,                    
                    WasCancelled = false,
                    
                });
                var jres =
                    Json(new { Success = true, Data = l, TransactionId = l}, JsonRequestBehavior.AllowGet);
                jres.MaxJsonLength = int.MaxValue;
                jres.RecursionLimit = 100;
                return jres;
            }
            catch (Exception ex)
            {
                return
                    Json(new { Success = false, Error = ex.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult PingTransactions(long transactionId)
        {
            try
            {
                var trans = _logic.SearchTransactions(new TransactionSearch
                {
                    Id = transactionId,
                    ProtectorId = -1,                    
                    PageSize =1                    
                }).SingleOrDefault();
                Coordinate coord = null;
                if (trans != null)
                {
                    var pro = trans.Protector;
                    if (pro != null)
                    {
                        coord = _logic.SearchCoordinates(new Search
                        {
                            PageSize = 1,
                            ProtectorId = pro.ProtectorId
                        }).SingleOrDefault();
                    }
                }
                double? avgRating = 5;
                try
                {
                    avgRating = trans?.Protector?.Ratings?.Average(x => x.RatingAmount);
                }
                catch(Exception ex)
                {

                }
                var jres = Json(new { Success = trans != null 
                    && trans.Protector != null,
                    Data = trans, Rating= avgRating,
                    Coordinates = coord}, JsonRequestBehavior.AllowGet);
                jres.MaxJsonLength = int.MaxValue;
                jres.RecursionLimit = 100;
                return jres;
            }
            catch(Exception ex)
            {
                return
                    Json(new {Success = false, Error = ex.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult CancelTransaction(long transactionId, string phoneNumber)
        {
            try
            {
                var trans = _logic.SearchTransactions(new TransactionSearch
                {
                    PageSize = 1,
                    Id = transactionId,
                    ProtecteePhoneNumber = phoneNumber
                });
                foreach(var t in trans)
                {
                    _logic.CancelTransaction(t.ProtectionTransactionId);
                }
                if(trans.Count == 0)
                {
                    return
                        Json(new { Success = false }, JsonRequestBehavior.AllowGet);
                }
                return
                    Json(new { Success= true}, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return
                    Json(new { Error = ex.ToString(), Success = false}, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult IsInCurrentSession(string phoneNumber, string protectorPhoneNumber, long transactionId)
        {
            try { 
                var transaction = _logic.SearchTransactions(new TransactionSearch
                {
                    Id = transactionId,
                    PageSize = 1,
                    ProtectorId = -1
                }).FirstOrDefault(x => x.Protector != null && !x.WasCancelled);
                if (transaction == null)
                {
                    return
                        Json(new { Success = false }, JsonRequestBehavior.AllowGet);
                }
                var jres =
                  Json(new { Success = transaction.AmountCharged > 100, AmountCharged = transaction.AmountCharged }, JsonRequestBehavior.AllowGet);
                jres.MaxJsonLength = int.MaxValue;
                jres.RecursionLimit = 100;
                return jres;
            }
            catch (Exception ex)
            {
                return 
            Json(new { Error = ex.ToString(), Success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult MakeTransaction(long transactionId, DateTime? startTime, DateTime? endTime, 
            string protectorPhoneNumber, string protecteePhoneNumber, decimal
            startLatitude, decimal startLongitude, decimal? endLatitude, decimal? endLongitude)
        {
            try
            {
                var sl = startLatitude.ToString();
                startLatitude = sl.Length > 7 ? decimal.Parse(sl.Substring(0, 7)) : startLatitude;
                var slo = startLongitude.ToString();
                startLongitude = slo.Length > 7 ? decimal.Parse(slo.Substring(0, 7)) : startLongitude;
                var tr = _logic.SearchTransactions(new TransactionSearch
                {
                    Id = transactionId,
                    PageSize = 1,
                    ProtectorId = -1
                }).Single();
                if(startTime == null || startTime == DateTime.MinValue)
                {
                    startTime = tr.DateCreated;
                }
                if(endTime == null || endTime == DateTime.MinValue)
                {
                    endTime = DateTime.UtcNow;
                }
                var protectee = _logic.SearchProtectees(new PersonDetailsSearch
                {
                    PhoneNumber = protecteePhoneNumber,
                    PageSize = 1
                }).Single();
                if (endLatitude == null && endLongitude == null)
                {
                    var latestCoords = _logic.SearchCoordinates(new Search
                    {
                        ProtecteeId = protectee.ProtecteeId,
                        PageSize = 25
                    }).OrderByDescending(x => x.CoordinateId).FirstOrDefault();
                    if(latestCoords != null)
                    {
                        endLongitude = latestCoords.Longitude;
                        endLatitude = latestCoords.Latitude;
                    }
                }
                var pro = _logic.SearchProtectors(new PersonDetailsSearch
                {
                    PageSize = 1,
                    PhoneNumber = protectorPhoneNumber
                }).Single();
                var c1Id = _logic.AddUpdateCoordinates(new Coordinate
                {
                    DateCreated
                     = startTime.Value,
                    Latitude = startLatitude,
                    Longitude = startLongitude,
                    Protectee = protectee,
                    Protector = pro
                });
                var c2Id = _logic.AddUpdateCoordinates(new Coordinate
                {
                    DateCreated= endTime.Value,
                    Protector = pro,
                    Protectee = protectee,
                    Latitude = endLatitude.Value,
                    Longitude = endLongitude.Value
                });
                var pay = _logic.SearchPayments(new Search
                {
                    ProtecteeId = protectee.ProtecteeId,
                    PageSize = int.MaxValue
                }).FirstOrDefault(x => !x.IsInvalid);
                if(pay == null)
                {
                    return
                        Json(new { Success = false, Error = "No Valid Payment Found!" }, JsonRequestBehavior.AllowGet);
                }
                var trans = _logic.MakeTransaction(new ProtectionTransaction
                {
                   ProtectionTransactionId = transactionId,
                    DateCreated = DateTime.UtcNow,
                    EndCoordinate = _logic.SearchCoordinates(new Search
                    {
                        Id = c2Id,
                        PageSize = 1
                    }).Single(),
                    EndDate = endTime.Value,
                   
                    StartCoordinate = _logic.SearchCoordinates(new Search
                    {
                        Id = c1Id,
                        PageSize = 1
                    }).Single(),
                    Protectee = protectee,
                    Protector = pro,
                    WasCancelled = false,
                    AmountCharged = 1,
                    PaymentId = pay.PaymentId,
                    ProtectionType = pro.ProtectorType
                });
                var jres = Json(new
                {
                    Data =
                    _logic.SearchTransactions(new TransactionSearch
                    {
                        Id = trans,
                        PageSize = 1,
                        ProtectorId = -1
                    }).Single(),
                    Success = true
                }, JsonRequestBehavior.AllowGet);
                jres.MaxJsonLength = int.MaxValue;
                jres.RecursionLimit = 100;
                return jres;
            }
            catch (Exception ex)
            {
                return
                    Json(new { Error = ex.ToString(), Success = false }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}