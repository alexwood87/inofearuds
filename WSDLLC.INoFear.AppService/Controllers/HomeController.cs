﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WSDLLC.INoFear.BusinessLogic;
using WSDLLC.INoFear.Core.Contracts;

namespace WSDLLC.INoFear.AppService.Controllers
{
    public class HomeController : Controller
    {
        private readonly IAdminBusinessLogic _adminLogic;
        public HomeController()
        {
            _adminLogic = new AdminBusinessLogic(ConfigurationManager.ConnectionStrings["ConnString"].ConnectionString);
        }
        public ActionResult Index()
        {
           return View();
        }
        [HttpGet]
        public ActionResult ClientPasswordReset()
        {
            return
                View();
        }
        [HttpPost]
        public ActionResult ClientPasswordReset(string phoneNumber)
        {
            try
            {
                _adminLogic.IssueResetCode(phoneNumber);
                return RedirectToAction("ResetPassword");
            }
            catch (Exception ex)
            {
                ViewBag.Message = ex.Message;
                return
                    View();
            }
        }
        [HttpGet]
        public ActionResult ResetClientPassword()
        {
            return
                View();
        }
        [HttpPost]
        public ActionResult ResetClientPassword(string phoneNumber, string code, string pin)
        {
            try
            {
                _adminLogic.ResetPassword(phoneNumber, code, pin);
                ViewBag.Message = "Pin Chanaged!";
                return
                    View();
            }
            catch(Exception ex)
            {
                ViewBag.Message = ex.Message;
                return View();
            }
        }
        [HttpGet]
        public ActionResult Login()
        {
            ViewBag.Message = "Admin Login";
            return
                View();
        }
        [HttpPost]
        public ActionResult Login(string username, string password)
        {
            var logic = new AdminBusinessLogic(ConfigurationManager.ConnectionStrings["ConnString"].ConnectionString);
            Session["User"] = logic.ValidateLogin(username, password);
            if(Session["User"] == null)
            {
                ViewBag.FriendlyMessage = "Invalid Username Or Password";
                return
                    View();
            }
            return
                Redirect("~/Admin/Index");
        }
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}