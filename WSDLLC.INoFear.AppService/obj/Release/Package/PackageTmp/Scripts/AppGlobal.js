﻿$(document).ready(function () {
    $('.datepicker').datepicker();
    $('.fancyboxiframeclose').fancybox({
        type: 'iframe',
        afterClose: function()
        {
            window.location.reload(true);
        }
    });
    $('.fancyboxiframe').fancybox({
        type: 'iframe'
    });
    $('.intsonly').blur(function () {
        var val = $(this).val();
        try{
            if (val != '' && parseInt(val) == NaN)
            {
                $(this).focus();
                alert('Only Whole Numbers Are Allowed');
            }
        }
        catch(ex)
        {
            alert('Only Whole Numbers Are Allowed');
            $(this).focus();
        }
    });
    $('.numbersonly').blur(function () {
        var val = $(this).val();
        try {
            if (val != '' && parseFloat(val) == NaN) {
                $(this).focus();
                alert('Only Whole Numbers Are Allowed');
            }
        }
        catch (ex) {
            alert('Only Whole Numbers Are Allowed');
            $(this).focus();
        }
    });
    $('.filter-table').filterTable({ filterExpression: 'filterTableFindAny' });
    $(".sort-table").tablesorter();

});
