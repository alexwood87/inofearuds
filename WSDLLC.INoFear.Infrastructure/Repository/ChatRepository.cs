﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WSDLLC.INoFear.Domain.Models;
using WSDLLC.INoFear.Domain.Repository;
using WSDLLC.INoFear.Domain.Entities;
namespace WSDLLC.INoFear.Infrastructure.Repository
{
    public class ChatRepository : IChatRepository
    {
        private INoFearEntities _context;
        public ChatRepository(INoFearEntities context)
        {
            _context = context;
        }
        public void AddChat(string chatMessage, DateTime createdDate, long protectorId, long protecteeId, bool isFromProtector)
        {
            _context.Chats.Add(new Chat
            {
                ChatId = Guid.NewGuid(),
                ChatMessage = chatMessage,
                CreatedDate = createdDate,
                ProtecteeId = protecteeId,
                ProtectorId = protectorId,
                AlreadySeen = false,
                IsFromProtector = isFromProtector
            });
            _context.SaveChanges();
        }

        public List<ChatModel> GetCurrentChats(long protecteeId, long protectorId, DateTime curTime, bool isFromProtector)
        {
            return _context.Chats.Where(c => c.IsFromProtector == isFromProtector 
            && c.ProtectorId == protectorId && c.ProtecteeId == protecteeId && c.CreatedDate >= curTime && c.AlreadySeen != true).ToList()
                .Select(c => new ChatModel
                {
                    ChatId = c.ChatId,
                    ChatMessage = c.ChatMessage,
                    CreatedDate = c.CreatedDate,
                    IsFromProtector = c.IsFromProtector
                }).ToList();

        }

        public void MarkChatSeen(Guid chatId)
        {
            var chat = _context.Chats.Single(c => c.ChatId == chatId);
            chat.AlreadySeen = true;
            _context.SaveChanges();
        }
    }
}
