﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WSDLLC.INoFear.Domain.Models;
using WSDLLC.INoFear.Domain.Repository;

namespace WSDLLC.INoFear.Infrastructure.Repository
{
    public class AdminRepository : IAdminRepository
    {
        private Domain.Entities.INoFearEntities _context;
        public AdminRepository(Domain.Entities.INoFearEntities context)
        {
            _context = context;
        }
        public void AddOrUpdateAdmin(AdminstratorModel admin)
        {
            var user = _context.Adminstrators.SingleOrDefault(x => x.Username.Trim().ToLower() == admin.Username.ToLower().Trim());
            if(user == null)
            {
                user = new Domain.Entities.Adminstrator
                {
                    Username = admin.Username,
                    Pin = admin.Pin,
                    AuthenticationLevel = (int) admin.AuthLevel
                };
                _context.Adminstrators.Add(user);
                _context.SaveChanges();
                return;
            }
            if (user.Pin != null)
            {
                user.Pin = admin.Pin;
            }
            user.AuthenticationLevel = (int)admin.AuthLevel;
        }

        public List<AdminstratorModel> AllAdministrators()
        {
            return
                _context.Adminstrators.Select(x => new AdminstratorModel
                {
                    AuthLevel = (AuthenticationLevel) x.AuthenticationLevel,
                    AdminstratorId = x.AdminstartorId,
                    Username = x.Username,
                    Pin = x.Pin
                }).ToList();
        }

        public void CertifyProtector(long protectorId)
        {
            var protector = _context.Protectors.SingleOrDefault(x => x.ProtectorId == protectorId);
            if(protector == null)
            {
                return;
            }
            protector.IsCertified = true;
            _context.SaveChanges();
        }
        public AdminstratorModel ValidateLogin(string username, string password)
        {
            var login = _context.Adminstrators.SingleOrDefault(x => x.Username.ToLower().Trim() == username.Trim().ToLower() &&
            x.Pin == password);
            if (login == null)
                return null;
            return
                new AdminstratorModel
                {
                    Username = login.Username,
                    AuthLevel = (AuthenticationLevel)login.AuthenticationLevel,
                    Pin = login.Pin,
                    AdminstratorId = login.AdminstartorId
                };
                
        }
        public void ChangeProtectorType(long protectorId, string type)
        {
            var protector = _context.Protectors.SingleOrDefault(x => x.ProtectorId == protectorId);
            if (protector == null)
            {
                return;
            }
            protector.ProtectorType = type;
            _context.SaveChanges();
        }

        public void UnCertifyProtector(long protectorId)
        {
            var protector = _context.Protectors.SingleOrDefault(x => x.ProtectorId == protectorId);
            if (protector == null)
            {
                return;
            }
            protector.IsCertified = false;
            _context.SaveChanges();
        }

        public List<Protector> SearchProtectors(bool ceritifed, int pageIndex, out int total)
        {
            total = _context.Protectors.Count(x => x.IsCertified == ceritifed);
            return
                _context.Protectors.Where(x => x.IsCertified == ceritifed).OrderByDescending(x => x.ProtectorType).
                   Skip(pageIndex * 50).Take(50).ToList().Select(Domain.Helpers.Mapper.MapProtector).ToList();
        }

        public void DeleteAdmin(int id)
        {
            var admin = _context.Adminstrators.SingleOrDefault(x => x.AdminstartorId == id);
            if(admin != null)
            {
                admin.IsDeleted = true;
                _context.SaveChanges();
            }
        }

        public string IssueResetCode(string phoneNumber)
        {
            string g = Guid.NewGuid().ToString();
            var dt = DateTime.UtcNow.AddHours(-2);
            var rcode = _context.ResetCodes.Where(x => x.PhoneNumber == phoneNumber && x.DateCreated > dt)
                .OrderByDescending(x => x.DateCreated).FirstOrDefault();
            if(rcode != null)
            {
                throw new Exception("You already requested a code: please wait two hours.");
            }
            var code = new Domain.Entities.ResetCode
            {
                Code = g,
                DateCreated = DateTime.UtcNow,
                PhoneNumber = phoneNumber
            };
            _context.ResetCodes.Add(code);
            _context.SaveChanges();
            return
                rcode.Code;
        }

        public void ResetPassword(string phoneNumber, string code, string pin)
        {
            var dt = DateTime.UtcNow.AddHours(-1);
            var rcode = _context.ResetCodes.Where(x => x.PhoneNumber == phoneNumber && x.Code == code && x.DateCreated > dt)
                .OrderByDescending(x => x.DateCreated).FirstOrDefault();
            if(code != null)
            {
                var protectee = _context.Protectees.SingleOrDefault(x => x.PhoneNumber == phoneNumber);
                if (protectee != null)
                {
                    protectee.Password = pin;
                    _context.SaveChanges();
                    return;
                }
                var protector = _context.Protectors.SingleOrDefault(x => x.PhoneNumber == phoneNumber);
                if(protector != null)
                {
                    protector.Password = pin;
                    _context.SaveChanges();
                    return;
                }
                                        
            }
            throw new Exception("Incorrect Information Entered!");
        }
    }
}
