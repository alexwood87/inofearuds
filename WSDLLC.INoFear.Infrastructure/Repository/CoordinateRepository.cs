﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WSDLLC.INoFear.Domain.Models;
using WSDLLC.INoFear.Domain.Repository;
namespace WSDLLC.INoFear.Infrastructure.Repository
{
    public class CoordinateRepository : ICoordinateRepository
    {
        private readonly Domain.Entities.INoFearEntities _context;
        public CoordinateRepository(Domain.Entities.INoFearEntities context)
        {
            _context = context;
        }
        public long AddUpdateCoordinates(Coordinate coordinate)
        {
            if(coordinate.CoordinateId == 0)
            {
                var c = new Domain.Entities.Coordinate
                {
                    DateCreated = coordinate.DateCreated,
                    Latitude = coordinate.Latitude,
                    Longitude = coordinate.Longitude,
                    ProtecteeId = coordinate.Protectee?.ProtecteeId,
                    ProtectorId = coordinate.Protector?.ProtectorId                    
                };
                _context.Coordinates.Add(c);
                _context.SaveChanges();
                return c.CoordinateId;
            }
            var c2 = _context.Coordinates.Single(x => x.CoordinateId == coordinate.CoordinateId);
            c2.Latitude = coordinate.Latitude;
            c2.Longitude = coordinate.Longitude;
            c2.ProtecteeId = coordinate.Protectee?.ProtecteeId;
            c2.ProtectorId = coordinate.Protector?.ProtectorId;
            _context.SaveChanges();
            return
                c2.CoordinateId;
        }

        public List<Coordinate> SearchCoordinates(Search search)
        {
            var coords = _context.Coordinates.AsQueryable();
            if(search.Id != null)
            {
                coords = coords.Where(x => x.CoordinateId == search.Id.Value);
            }
            if(search.ProtecteeId != null)
            {
                coords = coords.Where(x => x.ProtecteeId == search.ProtecteeId.Value);
            }
            if(search.ProtectorId != null)
            {
                coords = coords.Where(x => x.ProtectorId == search.ProtectorId.Value);
            }
            return
                coords.OrderByDescending(x => x.DateCreated).Skip(search.PageSize * search.PageIndex).Take(search.PageSize)
                    .ToList().Select(x => new Coordinate
                    {
                        CoordinateId = x.CoordinateId,
                        DateCreated = x.DateCreated,
                        Latitude = x.Latitude,
                        Longitude = x.Longitude,
                        Protectee = Domain.Helpers.Mapper.MapProtectee(x.Protectee),
                        Protector = Domain.Helpers.Mapper.MapProtector(x.Protector)
                    }).ToList();
        }
    }
}
