﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WSDLLC.INoFear.Domain.Models;
using WSDLLC.INoFear.Domain.Repository;
namespace WSDLLC.INoFear.Infrastructure.Repository
{
    public class ProtectionRepository : IProtectionRepository
    {
        private readonly Domain.Entities.INoFearEntities _context;
        public ProtectionRepository(Domain.Entities.INoFearEntities context)
        {
            _context = context;
        }
        public long Rate(Rating r)
        {
            if(r.Protector.ProtectorId == 0)
            {
                r.Protector.ProtectorId = _context.Protectors.Single(x => x.PhoneNumber == r.Protector.PhoneNumber).ProtectorId;
            }
            if(r.Protectee.ProtecteeId == 0)
            {
                r.Protectee.ProtecteeId = _context.Protectees.Single(x => x.PhoneNumber == r.Protectee.PhoneNumber).ProtecteeId;
            }
            var rating = new Domain.Entities.Rating
            {
                ProtecteeId = r.Protector.ProtectorId,
                ProtectorId = r.Protector.ProtectorId,
                Rating1 = r.RatingAmount,
                IsForProtectorRating = r.IsForProtector
            };
            _context.Ratings.Add(rating);
            _context.SaveChanges();
            return
                rating.RatingId;
        }

        public long RegisterProtectee(Protectee p)
        {
            if(p.ProtecteeId == 0)
            {
                var p2 = _context.Protectees.SingleOrDefault(x => x.PhoneNumber == p.PhoneNumber.Trim());
                if(p2 != null)
                {
                    return p2.ProtecteeId;
                }
                p2 = new Domain.Entities.Protectee
                {
                    FirstName = p.FirstName,
                    Password = p.Password,
                    LastName = p.LastName,
                    PhoneNumber = p.PhoneNumber,
                    Picture = p.Picture,
                    Username = p.Email             
                };
                _context.Protectees.Add(p2);
                _context.SaveChanges();
                return
                    p2.ProtecteeId;
            }
            return -1;
        }

        public long RegisterProtector(Protector p)
        {
            if (p.ProtectorId == 0)
            {
                var p2 = _context.Protectors.SingleOrDefault(x => x.PhoneNumber == p.PhoneNumber.Trim());
                if (p2 != null)
                {
                    p2.ProtectorType = p.ProtectorType;
                    p2.Picture = p.Picture;
                    p2.Password = p.Password;
                    p2.LastName = p.LastName;
                    p2.IsCertified = p.IsCertified;
                    p2.FirstName = p.FirstName;
                    p2.Username = p.Email;
                    _context.SaveChanges();
                    return p2.ProtectorId;
                }
                p2 = new Domain.Entities.Protector
                {
                    FirstName = p.FirstName,
                    Password = p.Password,
                    LastName = p.LastName,
                    PhoneNumber = p.PhoneNumber,
                    Picture = p.Picture,
                    Username = p.Email,
                    IsCertified = p.IsCertified,
                    ProtectorType = p.ProtectorType                    
                };
                _context.Protectors.Add(p2);
                _context.SaveChanges();
                return
                    p2.ProtectorId;
            }
            var p3 = _context.Protectors.Single(x => x.ProtectorId == p.ProtectorId);
         
            p3.ProtectorType = p.ProtectorType;
            p3.Picture = p.Picture;
            p3.Password = p.Password;
            p3.LastName = p.LastName;
            p3.IsCertified = p.IsCertified;
            p3.FirstName = p.FirstName;
            _context.SaveChanges();
            return
            p.ProtectorId;
        }

        public List<Protector> SearchProtectors(PersonDetailsSearch search)
        {
            var protectors = _context.Protectors.AsQueryable();
            if (search.Id != null)
            {
                protectors = protectors.Where(x => x.ProtectorId == search.Id.Value);
            }
            if (search.Latitude != null && protectors.Any(x => x.Coordinates.Any()))
            {
                protectors = protectors.Where(x => x.Coordinates.
                    OrderByDescending(t => t.DateCreated).First().Latitude == search.Latitude);
            }
            if (search.Longitude != null && protectors.Any(x => x.Coordinates.Any()))
            {
                protectors = protectors.Where(x => x.Coordinates.
                    OrderByDescending(t => t.DateCreated).First().Longitude == search.Longitude);
            }
            if (search.PhoneNumber != null)
            {
                protectors = protectors.Where(x => x.PhoneNumber == search.PhoneNumber.Trim());
            }
            if (search.ProtecteeId != null)
            {
                protectors = protectors.Where(x => x.ProtectionTransactions.Any(y => y.ProtecteeId == search.ProtecteeId.Value));
            }
            return
                protectors.OrderByDescending(x => x.LastName).ThenBy(x => x.FirstName).Skip(search.PageIndex * search.PageSize)
                    .Take(search.PageSize).ToList().Select(Domain.Helpers.Mapper.MapProtector).ToList();
        }

        public List<Rating> SearchRatings(Search search)
        {
            var ratings = _context.Ratings.AsQueryable();
            if(search.Id != null)
            {
                ratings = ratings.Where(x => x.RatingId == search.Id.Value);
            }
            if(search.ProtecteeId != null)
            {
                ratings = ratings.Where(x => x.ProtecteeId == search.ProtecteeId);
            }
            if (search.ProtectorId != null)
            {
                ratings = ratings.Where(x => x.ProtectorId == search.ProtectorId.Value);
            }
            return
               Domain.Helpers.Mapper.MapRatings(
                ratings.OrderByDescending(x => x.RatingId).Skip(search.PageSize * search.PageIndex).Take(search.PageSize).ToList());
        }
        public List<Protectee> SearchProtectees(PersonDetailsSearch search)
        {
            var protectees = _context.Protectees.AsQueryable();
            if(search.Id != null)
            {
                protectees = protectees.Where(x => x.ProtecteeId == search.Id.Value);
            }
            if(search.Latitude != null && protectees.Any(x => x.Coordinates.Any()))
            {
                protectees = protectees.Where(x => x.Coordinates.
                    OrderByDescending(t => t.DateCreated).First().Latitude == search.Latitude);
            }
            if (search.Longitude != null && protectees.Any(x => x.Coordinates.Any()))
            {
                protectees = protectees.Where(x => x.Coordinates.
                    OrderByDescending(t => t.DateCreated).First().Longitude == search.Longitude);
            }
            if(search.PhoneNumber != null)
            {
                protectees = protectees.Where(x => x.PhoneNumber == search.PhoneNumber.Trim());
            }
            if(search.ProtectorId != null)
            {
                protectees = protectees.Where(x => x.ProtectionTransactions.Any(y => y.ProtectorId == search.ProtectorId));
            }
            return
                protectees.OrderByDescending(x => x.LastName).ThenBy(x => x.FirstName).Skip(search.PageIndex * search.PageSize)
                    .Take(search.PageSize).ToList().Select(Domain.Helpers.Mapper.MapProtectee).ToList();
        }
    }
}
