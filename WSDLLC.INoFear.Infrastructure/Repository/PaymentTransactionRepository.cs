﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WSDLLC.INoFear.Domain.Models;
using WSDLLC.INoFear.Domain.Repository;
namespace WSDLLC.INoFear.Infrastructure.Repository
{
    public class PaymentTransactionRepository : IPaymentTransactionRepository
    {
        private readonly Domain.Entities.INoFearEntities _context;
        public PaymentTransactionRepository(Domain.Entities.INoFearEntities context)
        {
            _context = context;
        }
        public void CancelTransaction(long transactionId)
        {
            var trans = _context.ProtectionTransactions.Single(x => x.ProtectionTransactionId == transactionId);
            if (trans.AmountCharged == 0)
            {
                trans.WasCancelled = true;
                _context.SaveChanges();
            }
           
        }
        public long MakeTransaction(ProtectionTransaction p)
        {
            if(p.ProtectionTransactionId > 0)
            {
                var trans = _context.ProtectionTransactions.Single(x => x.ProtectionTransactionId == p.ProtectionTransactionId);
                trans.ProtecteeId = p.Protectee.ProtecteeId;
                trans.ProtectorId = p.Protector?.ProtectorId;
                trans.ProtectorType = p.ProtectionType;
                trans.WasCancelled = p.WasCancelled;
                trans.StartCoordinateId = p.StartCoordinate.CoordinateId;
                trans.DateCreated = p.DateCreated;
                trans.EndCoordinateId = p.EndCoordinate?.CoordinateId;
                trans.EndDate = p.EndDate == DateTime.MinValue ? (DateTime?) null : p.EndDate;
                trans.PaymentId = p.PaymentId;
                trans.AmountCharged = p.AmountCharged;
                _context.SaveChanges();
                return p.ProtectionTransactionId;
            }
            var p2 = new Domain.Entities.ProtectionTransaction
            {
                AmountCharged = p.AmountCharged,
                ProtecteeId = p.Protectee.ProtecteeId,
                ProtectorId = p.Protector?.ProtectorId,
                StartCoordinateId = p.StartCoordinate.CoordinateId,
                EndCoordinateId = p.EndCoordinate?.CoordinateId,
                DateCreated = p.DateCreated,
                WasCancelled = p.WasCancelled,
                EndDate = p.EndDate                ,
                ProtectorType = p.ProtectionType,
                PaymentId = p.PaymentId                
            };
            _context.ProtectionTransactions.Add(p2);
            _context.SaveChanges();
            return
                p2.ProtectionTransactionId;
        }

        public long RegisterPayment(Payment p, string phoneNumber)
        {
            var p2 = new Domain.Entities.Payment
            {
                CreditCardNumber = p.CreditCardNumber,
                ExperationMonth = p.ExperationMonth,
                ExperationYear = p.ExperationYear,
                NameOnCard = p.NameOnCard,
                SecurityCode = p.SecurityCode,
                IsInvalid = p.IsInvalid,
                CardType = p.CardType
            };
            var pay2 = _context.Payments.SingleOrDefault(x => x.CreditCardNumber == p.CreditCardNumber && x.ExperationMonth == p.ExperationMonth
            && x.ExperationYear == p.ExperationYear && x.NameOnCard == p.NameOnCard.Trim() && x.CardType == p.CardType);
            if (pay2 != null)
            {
                return pay2.PaymentId;
            }
            _context.Payments.Add(p2);
            _context.SaveChanges();
            var protectee = _context.Protectees.SingleOrDefault(x => x.PhoneNumber == phoneNumber);
            if (protectee == null)
            {
                var protector = _context.Protectors.Single(x => x.PhoneNumber == phoneNumber);
                var payToPro = new Domain.Entities.PaymentToProtector
                {
                    PaymentId = p2.PaymentId,
                    ProtectorId = protector.ProtectorId
                };
                _context.PaymentToProtectors.Add(payToPro);
                _context.SaveChanges();
            }
            else
            {
                var paymentsToCusts = new Domain.Entities.PaymentToProtectee
                {
                    ProtecteeId = protectee.ProtecteeId,
                    PaymentId = p2.PaymentId
                };

                _context.PaymentToProtectees.Add(paymentsToCusts);
                _context.SaveChanges();
            }
            return
                p2.PaymentId;
        }
        public bool DeletePayment(string phoneNumber, Payment p)
        {
            var pre = _context.Protectees.SingleOrDefault(x => x.PhoneNumber == phoneNumber);
            if (pre == null)
            {
                var pro = _context.Protectors.SingleOrDefault(x => x.PhoneNumber == phoneNumber);
                if(pro == null)
                {
                    return false;
                }
                var p2 = pro.PaymentToProtectors.FirstOrDefault(x => x.Payment.ExperationMonth == p.ExperationMonth &&
                    x.Payment.ExperationYear == p.ExperationYear && x.Payment.SecurityCode == p.SecurityCode
                    && x.Payment.CreditCardNumber == p.CreditCardNumber && x.Payment.CardType == p.CardType);

                if(p2 == null)
                {
                    return true;
                }
                //p2.Payment.IsInvalid = false;
                var p3 = _context.Payments.Single(c => c.PaymentId == p2.PaymentId);
                p3.IsInvalid = true;
                _context.SaveChanges();
                return
                    true;
            }
            var p4 = pre.PaymentToProtectees.FirstOrDefault(x => x.Payment.ExperationMonth == p.ExperationMonth &&
                    x.Payment.ExperationYear == p.ExperationYear && x.Payment.SecurityCode == p.SecurityCode
                     && x.Payment.CreditCardNumber == p.CreditCardNumber && x.Payment.CardType == p.CardType);
            if (p4 == null)
            {
                return true;
            }
            //p2.Payment.IsInvalid = false;
            var p5 = _context.Payments.Single(c => c.PaymentId == p4.PaymentId);
            p5.IsInvalid = true;
            _context.SaveChanges();
            return
                true;
        }
        public long RegisterPaymentForProtector(long protectorId, Payment pay)
        {
            var pay2 = _context.Payments.SingleOrDefault(x => x.CreditCardNumber == pay.CreditCardNumber 
            && x.ExperationMonth == pay.ExperationMonth
               && x.ExperationYear == pay.ExperationYear && x.NameOnCard == pay.NameOnCard.Trim() && x.CardType == pay.CardType);

            if (pay2 != null)
            {
                return
                    pay2.PaymentId;
            }
            pay2 = new Domain.Entities.Payment
            {
                CardType = pay.CardType,
                CreditCardNumber = pay.CreditCardNumber,
                ExperationMonth = pay.ExperationMonth,
                ExperationYear = pay.ExperationYear,
                IsInvalid = pay.IsInvalid,
                NameOnCard = pay.NameOnCard,
                PaymentId = pay.PaymentId,
                SecurityCode = pay.SecurityCode
            };
            _context.Payments.Add(pay2);
            _context.SaveChanges();
            var proToPay = new Domain.Entities.PaymentToProtector
            {
                PaymentId = pay2.PaymentId,
                ProtectorId = protectorId
            };
            _context.PaymentToProtectors.Add(proToPay);
            _context.SaveChanges();
            return
                pay2.PaymentId;
        }

        public List<Payment> SearchPayments(Search search)
        {
            var payments = _context.Payments.AsQueryable();
            if(search.Id != null)
            {
                payments = payments.Where(x => x.PaymentId == search.Id);
            }
            if(search.ProtecteeId != null)
            {
                payments = payments.Where(x => x.PaymentToProtectees.Any(y => y.ProtecteeId == search.ProtecteeId.Value));
            }
            if(search.ProtectorId != null)
            {
                payments = payments.Where(x => x.PaymentToProtectors.Any(y => y.ProtectorId == search.ProtectorId.Value));
            }
            return
                payments.Where(x => !x.IsInvalid).OrderByDescending(x => x.CreditCardNumber).Skip(search.PageIndex * search.PageSize).Take(search.PageSize)
                    .Select(x => new Payment
                    {
                        CreditCardNumber = x.CreditCardNumber,
                        ExperationMonth = x.ExperationMonth,
                        ExperationYear = x.ExperationYear,
                        NameOnCard = x.NameOnCard,
                        PaymentId = x.PaymentId,
                        SecurityCode = x.SecurityCode,
                        IsInvalid = x.IsInvalid,
                        CardType = x.CardType
                    }).ToList();
        }

        public List<ProtectionTransaction> SearchTransactions(TransactionSearch search)
        {
            var query = _context.ProtectionTransactions.AsQueryable();
            if(search.DateCreated > DateTime.MinValue)
            {
                query = query.Where(x => x.DateCreated >= search.DateCreated);
            }
            if(search.Id != null)
            {
                query = query.Where(x => x.ProtectionTransactionId == search.Id.Value);
            }
            if(search.ProtecteeId != null)
            {
                query = query.Where(x => x.ProtecteeId == search.ProtecteeId.Value);
            }
            if(search.ProtectorId != null)
            {
                if (search.ProtectorId != -1)
                {
                    query = query.Where(x => x.ProtectorId == search.ProtectorId.Value);
                }
            }
            else
            {
                query = query.Where(x => x.Protector == null);
            }
            if(!string.IsNullOrEmpty(search.ProtectorType))
            {
                query = query.Where(x => x.ProtectorType.ToLower() == search.ProtectorType.ToLower());
            }
            if(search.Latitude.HasValue && search.Longitude.HasValue)
            {
                var qu = query.AsQueryable();
                foreach(Domain.Entities.ProtectionTransaction trans in qu)
                {
                    if(trans.Coordinate1 == null)
                    {
                        query = query.Where(x => x.ProtectionTransactionId != trans.ProtectionTransactionId);
                        continue;
                    }
                    double R = 6371000.0;
                    double lat1 = (double) trans.Coordinate1.Latitude;
                    double lat2 = (double) search.Latitude;
                    double lon1 = (double) trans.Coordinate1.Latitude;
                    double lon2 = (double) search.Longitude;
                    double tf = Math.PI * (lat2 - lat1) / 180.0;
                    double tl = Math.PI * (lon2 - lon1) / 180.0;
                    double a = Math.Sin(tl / 2.0) * Math.Sin(tf / 2.0) * Math.Cos(lat1) * Math.Sin(tl / 2.0) * Math.Sin(tl);
                    double c = 2.0 * Math.Atan2(Math.Sqrt(Math.Abs(a)), Math.Sqrt(Math.Abs(1.0 - a)));
                    var o = (c * R)/ 1610.0;
                    if(o > 25.0)
                    {
                        query = query.Where(x => x.ProtectionTransactionId != trans.ProtectionTransactionId);
                    }
                    
                }
            }
            else if(search.Latitude.HasValue)
            {
                query = query.Where(x => x.Coordinate1 != null && Math.Abs(x.Coordinate1.Latitude - search.Latitude.Value) < (decimal)0.5);
            }
            else if(search.Longitude.HasValue)
            {
                query = query.Where(x => x.Coordinate1 != null && Math.Abs(x.Coordinate1.Longitude - search.Longitude.Value)<(decimal)0.5);
            }

            if(search.ProtecteePhoneNumber != null)
            {
                query = query.Where(x => x.Protectee != null && x.Protectee.PhoneNumber == search.ProtecteePhoneNumber);
            }
            if(search.ProtectorPhoneNumber != null)
            {
                query = query.Where(x => x.Protector != null && x.Protector.PhoneNumber == search.ProtectorPhoneNumber);
            }
            if(search.Amount != null)
            {
                query = query.Where(x => x.AmountCharged == search.Amount.Value);
            }
            if(search.WasCancelled != null)
            {
                query = query.Where(x => x.WasCancelled == search.WasCancelled.Value);
            }
            var qu2 = query.AsQueryable().OrderByDescending(x => x.ProtectionTransactionId);
            HashSet<long> ids = new HashSet<long>();
            foreach (var q1 in qu2)
            {
                if(!ids.Contains(q1.ProtecteeId))
                {
                    ids.Add(q1.ProtecteeId);
                }
                else
                {
                    query = query.Where(x => x.ProtectionTransactionId != q1.ProtectionTransactionId);
                }
            }
            var q =
               query.OrderByDescending(x => x.DateCreated).Skip(search.PageIndex * search.PageSize).Take(search.PageSize)
                  .ToList();
            
                   return q.Select(x => new ProtectionTransaction
                   {
                       AmountCharged = x.AmountCharged,
                       DateCreated = x.DateCreated,
                       EndCoordinate = x.Coordinate != null ? new Coordinate
                       {
                           DateCreated = x.Coordinate.DateCreated,
                           CoordinateId = x.Coordinate.CoordinateId,
                           Latitude = x.Coordinate.Latitude,
                           Longitude = x.Coordinate.Longitude  
                       } : null,
                       EndDate = x.EndDate.GetValueOrDefault(DateTime.MinValue),
                       Protectee = Domain.Helpers.Mapper.MapProtectee(x.Protectee),
                       Protector = Domain.Helpers.Mapper.MapProtector(x.Protector),
                       WasCancelled = x.WasCancelled,
                       StartCoordinate = x.Coordinate1 != null? new Coordinate
                       {
                           CoordinateId = x.Coordinate1.CoordinateId,
                           DateCreated = x.Coordinate1.DateCreated,
                           Latitude = x.Coordinate1.Latitude,
                           Longitude = x.Coordinate1.Longitude                           
                       } : null,
                       ProtectionTransactionId = x.ProtectionTransactionId,
                       ProtectionType = x.ProtectorType
                                             
                   }).OrderByDescending(
                       x => x.DateCreated).Skip(search.PageIndex*search.PageSize).Take(search.PageSize).ToList();
        }

        public void UpdatePayment(Payment p)
        {
            var p2 = _context.Payments.SingleOrDefault(x => x.PaymentId == p.PaymentId);
            if(p2 == null)
            {
                return;
            }
            p2.IsInvalid = p.IsInvalid;
            p2.NameOnCard = p.NameOnCard;
            p2.SecurityCode = p.SecurityCode;
            p2.ExperationMonth = p.ExperationMonth;
            p2.ExperationYear = p.ExperationYear;
            p2.CardType = p.CardType;
            p2.CreditCardNumber = p.CreditCardNumber;
            _context.SaveChanges();
        }
    }
}
