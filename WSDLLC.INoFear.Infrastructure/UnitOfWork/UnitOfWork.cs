﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WSDLLC.INoFear.Domain.Repository;
using WSDLLC.INoFear.Domain.UnitOfWork;
using WSDLLC.INoFear.Infrastructure.Repository;

namespace WSDLLC.INoFear.Infrastructure.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private ICoordinateRepository _coordinateRepository;
        private IPaymentTransactionRepository _paymentTransactionRepository;
        private IProtectionRepository _protectionRepository;
        private IAdminRepository _adminRepository;
        private IChatRepository _chatRepository;
        private readonly Domain.Entities.INoFearEntities _context;
        public UnitOfWork(string connString)
        {
            _context = new Domain.Entities.INoFearEntities(connString);
        }
        public IChatRepository ChatRepository
        {
            get
            {
                return
                    _chatRepository ?? (_chatRepository = new ChatRepository(_context));
            }
            set
            {
                _chatRepository = value;
            }
        }
        public IAdminRepository AdminRepository
        {
            get
            {
                return _adminRepository ?? (_adminRepository = new AdminRepository(_context));
            }
            set {
                _adminRepository = value;
            }
        }
        public ICoordinateRepository CoordinateRepository
        {
            get
            {
                return
                   (_coordinateRepository ?? (_coordinateRepository = new CoordinateRepository(_context)));
            }
            set
            {
                _coordinateRepository = value;
            }
        }

        public IPaymentTransactionRepository PaymentTransactionRepository
        {
            get
            {
                return
                    _paymentTransactionRepository ?? (_paymentTransactionRepository = new PaymentTransactionRepository(_context));
            }

            set
            {
                _paymentTransactionRepository = value;
            }
        }

        public IProtectionRepository ProtectionRepository
        {
            get
            {
                return
                   _protectionRepository ?? (_protectionRepository = new ProtectionRepository(_context));
            }

            set
            {
                _protectionRepository = value;
            }
        }
    }
}
