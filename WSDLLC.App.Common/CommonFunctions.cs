﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Net;

namespace WSDLLC.App.Common
{
    public class CommonFunctions
    {
        public static DataSet ParseExcelFile(string path, string[] sheetNames)
        {
            using (OleDbConnection conn = new OleDbConnection())
            {
                var ds = new DataSet();
                foreach (var sheetName in sheetNames)
                {
                    DataTable dt = new DataTable(sheetName);
                    string Import_FileName = path;
                    string fileExtension = Path.GetExtension(Import_FileName);
                    if (fileExtension == ".xls")
                        conn.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Import_FileName + ";" + "Extended Properties='Excel 8.0;HDR=YES;'";
                    if (fileExtension == ".xlsx")
                        conn.ConnectionString = "Provider=Microsoft.Ace.OLEDB.12.0;Data Source=" + Import_FileName + ";" + "Extended Properties='Excel 16.0 Xml;HDR=YES;'";
                    using (OleDbCommand comm = new OleDbCommand())
                    {
                        comm.CommandText = "Select * from [" + sheetName + "]";

                        comm.Connection = conn;

                        using (OleDbDataAdapter da = new OleDbDataAdapter())
                        {
                            da.SelectCommand = comm;
                            da.Fill(dt);
                            ds.Tables.Add(dt);
                        }

                    }
                }
                return ds;
            }

        }
        public static void Log(string type, string logpathFile, string message)
        {
            var m = string.Format("{0}: {1}", type, message);
            if (!Directory.Exists(logpathFile))
            {
                Directory.CreateDirectory(logpathFile);
            }

            File.WriteAllText(logpathFile + "\\log_" + DateTime.UtcNow.ToFileTime() + ".txt", message);
        }
        public static void SendEmail(string from, string to, string message, string title, string username, string password)
        {
            var client = new System.Net.Mail.SmtpClient();//add custom port here
                                                          //This object stores the authentication values

            client.UseDefaultCredentials = false;
            client.Credentials = new NetworkCredential(username, password);
            client.Host = "smtp.office365.com";
            client.EnableSsl = true;
            client.TargetName = "STARTTLS/smtp.office365.com";
            client.Port = 587;



            System.Net.Mail.MailMessage messageM = new System.Net.Mail.MailMessage(from, to, title, message);
            messageM.IsBodyHtml = true;
            client.Send(messageM);
        }
        public static int SendSMS(string AccountID, string Email, string Password, string Recipient, string Message, string providerUrl)
        {
            WebClient Client = new WebClient();
            string RequestURL, RequestData;

            RequestURL = providerUrl;

            RequestData = "AccountId=" + AccountID
                + "&Email=" + System.Web.HttpUtility.UrlEncode(Email)
                + "&Password=" + System.Web.HttpUtility.UrlEncode(Password)
                + "&Recipient=" + System.Web.HttpUtility.UrlEncode(Recipient)
                + "&Message=" + System.Web.HttpUtility.UrlEncode(Message);

            byte[] PostData = Encoding.ASCII.GetBytes(RequestData);
            byte[] Response = Client.UploadData(RequestURL, PostData);

            string Result = Encoding.ASCII.GetString(Response);
            int ResultCode = Convert.ToInt32(Result.Substring(0, 4));

            return ResultCode;
        }


    }
}
