USE [UDS]
GO
/****** Object:  Table [dbo].[Adminstrators]    Script Date: 9/8/2017 6:19:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Adminstrators](
	[AdminstartorId] [int] IDENTITY(1,1) NOT NULL,
	[Username] [nvarchar](100) NOT NULL,
	[Pin] [nvarchar](100) NOT NULL,
	[AuthenticationLevel] [int] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[AdminstartorId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[Chats]    Script Date: 9/8/2017 6:19:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Chats](
	[ChatId] [uniqueidentifier] NOT NULL,
	[ChatMessage] [nvarchar](2000) NOT NULL,
	[ProtectorId] [bigint] NOT NULL,
	[ProtecteeId] [bigint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[IsFromProtector] [bit] NOT NULL,
	[AlreadySeen] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[ChatId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[Coordinates]    Script Date: 9/8/2017 6:19:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Coordinates](
	[CoordinateId] [bigint] IDENTITY(1,1) NOT NULL,
	[Latitude] [decimal](15, 10) NOT NULL,
	[Longitude] [decimal](15, 10) NOT NULL,
	[ProtecteeId] [bigint] NULL,
	[ProtectorId] [bigint] NULL,
	[DateCreated] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[CoordinateId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[Payments]    Script Date: 9/8/2017 6:19:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Payments](
	[PaymentId] [bigint] IDENTITY(1,1) NOT NULL,
	[CreditCardNumber] [nvarchar](100) NULL,
	[SecurityCode] [nvarchar](100) NOT NULL,
	[ExperationYear] [nvarchar](10) NOT NULL,
	[ExperationMonth] [nvarchar](10) NOT NULL,
	[NameOnCard] [nvarchar](100) NOT NULL,
	[IsInvalid] [bit] NOT NULL,
	[CardType] [nvarchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[PaymentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[PaymentToProtectee]    Script Date: 9/8/2017 6:19:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PaymentToProtectee](
	[PaymentToProtecteeId] [bigint] IDENTITY(1,1) NOT NULL,
	[PaymentId] [bigint] NOT NULL,
	[ProtecteeId] [bigint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[PaymentToProtecteeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[PaymentToProtector]    Script Date: 9/8/2017 6:19:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PaymentToProtector](
	[PaymentToProtectorId] [bigint] IDENTITY(1,1) NOT NULL,
	[ProtectorId] [bigint] NOT NULL,
	[PaymentId] [bigint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[PaymentToProtectorId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[Protectees]    Script Date: 9/8/2017 6:19:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Protectees](
	[ProtecteeId] [bigint] IDENTITY(1,1) NOT NULL,
	[Username] [nvarchar](100) NOT NULL,
	[PhoneNumber] [nvarchar](100) NOT NULL,
	[FirstName] [nvarchar](100) NOT NULL,
	[LastName] [nvarchar](100) NOT NULL,
	[Password] [nvarchar](100) NOT NULL,
	[Picture] [image] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ProtecteeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[ProtectionTransaction]    Script Date: 9/8/2017 6:19:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProtectionTransaction](
	[ProtectionTransactionId] [bigint] IDENTITY(1,1) NOT NULL,
	[ProtectorId] [bigint] NULL,
	[ProtecteeId] [bigint] NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[AmountCharged] [decimal](20, 2) NOT NULL,
	[PaymentId] [bigint] NOT NULL,
	[WasCancelled] [bit] NOT NULL,
	[EndCoordinateId] [bigint] NULL,
	[StartCoordinateId] [bigint] NOT NULL,
	[EndDate] [datetime] NULL,
	[ProtectorType] [nvarchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ProtectionTransactionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[Protectors]    Script Date: 9/8/2017 6:19:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Protectors](
	[ProtectorId] [bigint] IDENTITY(1,1) NOT NULL,
	[FirstName] [nvarchar](100) NOT NULL,
	[LastName] [nvarchar](100) NULL,
	[Picture] [image] NOT NULL,
	[Username] [nvarchar](100) NOT NULL,
	[Password] [nvarchar](100) NOT NULL,
	[PhoneNumber] [nvarchar](100) NOT NULL,
	[IsCertified] [bit] NOT NULL,
	[ProtectorType] [nvarchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ProtectorId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[Rating]    Script Date: 9/8/2017 6:19:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Rating](
	[RatingId] [bigint] IDENTITY(1,1) NOT NULL,
	[ProtectorId] [bigint] NOT NULL,
	[Rating] [int] NOT NULL,
	[ProtecteeId] [bigint] NOT NULL,
	[IsForProtectorRating] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[RatingId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[ResetCodes]    Script Date: 9/8/2017 6:19:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ResetCodes](
	[ResetCodeId] [bigint] IDENTITY(1,1) NOT NULL,
	[Code] [nvarchar](1000) NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[PhoneNumber] [nvarchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ResetCodeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[ScheduledTransactions]    Script Date: 9/8/2017 6:19:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ScheduledTransactions](
	[ScheduledTransactionId] [bigint] IDENTITY(1,1) NOT NULL,
	[ProtectorId] [bigint] NULL,
	[ProtecteeId] [bigint] NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[DateScheduled] [datetime] NOT NULL,
	[WasCancelled] [bit] NOT NULL,
	[Notes] [nvarchar](1000) NULL,
PRIMARY KEY CLUSTERED 
(
	[ScheduledTransactionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
ALTER TABLE [dbo].[Adminstrators] ADD  DEFAULT ((1)) FOR [AuthenticationLevel]
GO
ALTER TABLE [dbo].[Adminstrators] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[Coordinates] ADD  DEFAULT (getdate()) FOR [DateCreated]
GO
ALTER TABLE [dbo].[Payments] ADD  DEFAULT ((0)) FOR [IsInvalid]
GO
ALTER TABLE [dbo].[ProtectionTransaction] ADD  DEFAULT (getdate()) FOR [DateCreated]
GO
ALTER TABLE [dbo].[ProtectionTransaction] ADD  DEFAULT ((0)) FOR [AmountCharged]
GO
ALTER TABLE [dbo].[ProtectionTransaction] ADD  DEFAULT ((0)) FOR [WasCancelled]
GO
ALTER TABLE [dbo].[Protectors] ADD  DEFAULT ((0)) FOR [IsCertified]
GO
ALTER TABLE [dbo].[Rating] ADD  DEFAULT ((1)) FOR [IsForProtectorRating]
GO
ALTER TABLE [dbo].[ScheduledTransactions] ADD  DEFAULT ((0)) FOR [WasCancelled]
GO
ALTER TABLE [dbo].[Chats]  WITH CHECK ADD FOREIGN KEY([ProtecteeId])
REFERENCES [dbo].[Protectees] ([ProtecteeId])
GO
ALTER TABLE [dbo].[Chats]  WITH CHECK ADD FOREIGN KEY([ProtectorId])
REFERENCES [dbo].[Protectors] ([ProtectorId])
GO
ALTER TABLE [dbo].[Coordinates]  WITH CHECK ADD FOREIGN KEY([ProtectorId])
REFERENCES [dbo].[Protectors] ([ProtectorId])
GO
ALTER TABLE [dbo].[Coordinates]  WITH CHECK ADD FOREIGN KEY([ProtecteeId])
REFERENCES [dbo].[Protectees] ([ProtecteeId])
GO
ALTER TABLE [dbo].[PaymentToProtectee]  WITH CHECK ADD FOREIGN KEY([PaymentId])
REFERENCES [dbo].[Payments] ([PaymentId])
GO
ALTER TABLE [dbo].[PaymentToProtectee]  WITH CHECK ADD FOREIGN KEY([ProtecteeId])
REFERENCES [dbo].[Protectees] ([ProtecteeId])
GO
ALTER TABLE [dbo].[PaymentToProtector]  WITH CHECK ADD FOREIGN KEY([PaymentId])
REFERENCES [dbo].[Payments] ([PaymentId])
GO
ALTER TABLE [dbo].[PaymentToProtector]  WITH CHECK ADD FOREIGN KEY([ProtectorId])
REFERENCES [dbo].[Protectors] ([ProtectorId])
GO
ALTER TABLE [dbo].[ProtectionTransaction]  WITH CHECK ADD FOREIGN KEY([EndCoordinateId])
REFERENCES [dbo].[Coordinates] ([CoordinateId])
GO
ALTER TABLE [dbo].[ProtectionTransaction]  WITH CHECK ADD FOREIGN KEY([PaymentId])
REFERENCES [dbo].[Payments] ([PaymentId])
GO
ALTER TABLE [dbo].[ProtectionTransaction]  WITH CHECK ADD FOREIGN KEY([ProtectorId])
REFERENCES [dbo].[Protectors] ([ProtectorId])
GO
ALTER TABLE [dbo].[ProtectionTransaction]  WITH CHECK ADD FOREIGN KEY([ProtecteeId])
REFERENCES [dbo].[Protectees] ([ProtecteeId])
GO
ALTER TABLE [dbo].[ProtectionTransaction]  WITH CHECK ADD FOREIGN KEY([StartCoordinateId])
REFERENCES [dbo].[Coordinates] ([CoordinateId])
GO
ALTER TABLE [dbo].[Rating]  WITH CHECK ADD FOREIGN KEY([ProtecteeId])
REFERENCES [dbo].[Protectees] ([ProtecteeId])
GO
ALTER TABLE [dbo].[Rating]  WITH CHECK ADD FOREIGN KEY([ProtectorId])
REFERENCES [dbo].[Protectors] ([ProtectorId])
GO
ALTER TABLE [dbo].[ScheduledTransactions]  WITH CHECK ADD FOREIGN KEY([ProtectorId])
REFERENCES [dbo].[Protectors] ([ProtectorId])
GO
ALTER TABLE [dbo].[ScheduledTransactions]  WITH CHECK ADD FOREIGN KEY([ProtecteeId])
REFERENCES [dbo].[Protectees] ([ProtecteeId])
GO
