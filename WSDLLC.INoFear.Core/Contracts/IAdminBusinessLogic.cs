﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WSDLLC.INoFear.Domain.Models;

namespace WSDLLC.INoFear.Core.Contracts
{
    public interface IAdminBusinessLogic
    {
        void AddOrUpdateAdmin(AdminstratorModel admin, AuthenticationLevel level);
        List<AdminstratorModel> AllAdministrators(AuthenticationLevel level);
        void CertifyProtector(long protectorId);
        void UnCertifyProtector(long protectorId);
        void SendStatusEmail(long? protectorId, long? protecteeId, string message, string title);

        void ChangeProtectorType(long protectorId, string type);
        AdminstratorModel ValidateLogin(string username, string password);
        List<Protector> SearchProtectors(bool ceritifed, int pageIndex, out int total);
        void DeleteAdmin(int id, AuthenticationLevel level);
        void IssueResetCode(string phoneNumber);
        void ResetPassword(string phoneNumber, string code, string pin);
    }
}
