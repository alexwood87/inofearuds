﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WSDLLC.INoFear.Domain.Models;

namespace WSDLLC.INoFear.Core.Contracts
{
    public interface IAppBusinessLogic
    {
        ChatModel GetMostCurrentChat(string protectorPhone, string protecteePhone, bool isFromProtector);
        void AddChat(string message, string protectorPhone, string protecteePhone, bool isFromProtector);
        List<Coordinate> SearchCoordinates(Search search);
        long AddUpdateCoordinates(Coordinate coordinate);
        long RegisterPayment(Payment p, string phoneNumber);
        long MakeTransaction(ProtectionTransaction p);
        List<Payment> SearchPayments(Search search);
        List<ProtectionTransaction> SearchTransactions(TransactionSearch search);
        long RegisterProtector(Protector p, Payment pay);
        long RegisterProtectee(Protectee p, Payment pay);
        List<Protectee> SearchProtectees(PersonDetailsSearch search);
        List<Protector> SearchProtectors(PersonDetailsSearch search);
        long Rate(Rating r);
        bool DeletePayment(string phoneNumber, Payment p);
        List<Rating> SearchRatings(string protectorPhone, long? ratingId);
        void CancelTransaction(long transactionId);
    }
}
