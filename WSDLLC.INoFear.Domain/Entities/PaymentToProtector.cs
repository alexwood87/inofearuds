//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WSDLLC.INoFear.Domain.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class PaymentToProtector
    {
        public long PaymentToProtectorId { get; set; }
        public long ProtectorId { get; set; }
        public long PaymentId { get; set; }
    
        public virtual Payment Payment { get; set; }
        public virtual Protector Protector { get; set; }
    }
}
