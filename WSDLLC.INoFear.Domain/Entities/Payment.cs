//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WSDLLC.INoFear.Domain.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class Payment
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Payment()
        {
            this.PaymentToProtectees = new HashSet<PaymentToProtectee>();
            this.PaymentToProtectors = new HashSet<PaymentToProtector>();
            this.ProtectionTransactions = new HashSet<ProtectionTransaction>();
        }
    
        public long PaymentId { get; set; }
        public string CreditCardNumber { get; set; }
        public string SecurityCode { get; set; }
        public string ExperationYear { get; set; }
        public string ExperationMonth { get; set; }
        public string NameOnCard { get; set; }
        public bool IsInvalid { get; set; }
        public string CardType { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PaymentToProtectee> PaymentToProtectees { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PaymentToProtector> PaymentToProtectors { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProtectionTransaction> ProtectionTransactions { get; set; }
    }
}
