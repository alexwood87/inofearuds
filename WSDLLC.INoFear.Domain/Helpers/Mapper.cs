﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WSDLLC.INoFear.Domain.Helpers
{
    public class Mapper
    {
        public static Models.Protector MapProtector(Entities.Protector p)
        {
            if (p == null)
                return null;
            return
                new Models.Protector
                {
                    FirstName = p.FirstName,
                    LastName = p.LastName,
                    Password = p.Password,
                    PhoneNumber = p.PhoneNumber,
                    Picture = p.Picture,
                    ProtectorId = p.ProtectorId,
                    Username = p.Username,
                    Ratings = MapRatings(p.Ratings),
                    IsCertified = p.IsCertified,
                    ProtectorType = p.ProtectorType,
                    Payments = p.PaymentToProtectors?.Where(x => x.Payment != null).Select(x => new Models.Payment
                    {
                        CreditCardNumber = x.Payment.CreditCardNumber,
                        ExperationMonth = x.Payment.ExperationMonth,
                        ExperationYear = x.Payment.ExperationYear,
                        IsInvalid = x.Payment.IsInvalid,
                        NameOnCard = x.Payment.NameOnCard,
                        PaymentId = x.PaymentId,
                        SecurityCode = x.Payment.SecurityCode
                    }).ToList(),
                    Rating =(int) (p.Ratings == null || p.Ratings.Count == 0 ? 5.0 : p?.Ratings?.Average(c => c.Rating1) ?? 5.0)
                };
        } 
        public static List<Models.Rating> MapRatings(ICollection<Entities.Rating> ratings )
        {
            if (ratings == null)
                return null;
            return
                ratings.Select(x => new Models.Rating
                {
                    RatingAmount = x.Rating1,
                    IsForProtector = x.IsForProtectorRating
                }).ToList();
        } 
        public static Models.Protectee MapProtectee(Entities.Protectee p)
        {
            if (p == null)
                return null;
            return new Models.Protectee
            {
                FirstName = p.FirstName,
                LastName = p.LastName,
                Password = p.Password,
                PhoneNumber = p.PhoneNumber,
                Picture = p.Picture,
                ProtecteeId = p.ProtecteeId,
                Username = p.Username,
                Email = p.Username,
                Rating = (int) (p.Ratings == null || p.Ratings.Count == 0 ? 5.0 :  p?.Ratings?.Average(c => c.Rating1) ?? 5.0)
            
            };
        }
    }
}
