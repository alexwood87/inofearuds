﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WSDLLC.INoFear.Domain.Repository;
namespace WSDLLC.INoFear.Domain.UnitOfWork
{
    public interface IUnitOfWork
    {
        ICoordinateRepository CoordinateRepository { get; set; }
        IPaymentTransactionRepository PaymentTransactionRepository { get; set; }
        IProtectionRepository ProtectionRepository { get; set; } 
        IAdminRepository AdminRepository { get; set; }
        IChatRepository ChatRepository { get; set; }
    }
}
