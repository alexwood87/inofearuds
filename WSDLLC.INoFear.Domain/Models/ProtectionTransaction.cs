﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WSDLLC.INoFear.Domain.Models
{
    [DataContract]
    public class ProtectionTransaction
    {
        [DataMember]
        public long ProtectionTransactionId { get; set; }
        [DataMember]
        public DateTime DateCreated { get; set; }
        [DataMember]
        public DateTime EndDate { get; set; }
        [DataMember]
        public Protectee Protectee { get; set; }
        [DataMember]
        public Protector Protector { get; set; }
        [DataMember]
        public decimal AmountCharged { get; set; }
        [DataMember]
        public bool WasCancelled { get; set; }
        [DataMember]
        public Coordinate StartCoordinate { get; set; }
        [DataMember]
        public Coordinate EndCoordinate { get; set; } 
        [DataMember]
        public string ProtectionType { get; set; }
        [DataMember]
        public long PaymentId { get; set; }
    }
}
