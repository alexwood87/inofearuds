﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WSDLLC.INoFear.Domain.Models
{
    [DataContract]
    public enum AuthenticationLevel
    {
        [EnumMember]
        SUPERADMIN = 0,
        [EnumMember]
        ADMIN = 1,
        [EnumMember]
        MODERATOR=2
    }
}
