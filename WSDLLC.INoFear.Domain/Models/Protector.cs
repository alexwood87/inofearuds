﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WSDLLC.INoFear.Domain.Models
{
    [DataContract]
    public class Protector
    {
        [DataMember]
        public long ProtectorId { get; set; }
        [DataMember]
        public string FirstName { get; set; }
        [DataMember]
        public string LastName { get; set; }
        [DataMember]
        public byte[] Picture { get; set; }
        [DataMember]
        public string Username { get; set; }
        [DataMember]
        public string Password { get; set; }
        [DataMember]
        public string PhoneNumber { get; set; }
        [DataMember]
        public bool IsCertified { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public string ProtectorType { get; set; }
        [DataMember]
        public List<Rating> Ratings { get; set; }
        [DataMember]
        public int Rating { get; set; }
        [DataMember]
        public List<Payment> Payments { get; set; }
    }
}
