﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WSDLLC.INoFear.Domain.Models
{
    [DataContract]
    public class ChatModel
    {
        [DataMember]
        public Guid ChatId { get; set; }
        [DataMember]
        public string ChatMessage { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public Protectee Protectee { get; set; }
        [DataMember]
        public Protector Protector { get; set; }
        [DataMember]
        public bool IsFromProtector { get; set; }

    }
}
