﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WSDLLC.INoFear.Domain.Models
{
    [DataContract]
    public class TransactionSearch : Search
    {
        [DataMember]
        public DateTime DateCreated { get; set; }
        [DataMember]
        public string ProtecteePhoneNumber { get; set; }
        [DataMember]
        public string ProtectorPhoneNumber { get; set; }
        [DataMember]
        public decimal? Latitude { get; set; }
        [DataMember]
        public decimal? Longitude { get; set; }

        [DataMember]
        public decimal? Amount { get; set; }
        [DataMember]
        public string ProtectorType { get; set; }
        [DataMember]
        public bool? WasCancelled { get; set; }


    }
}
