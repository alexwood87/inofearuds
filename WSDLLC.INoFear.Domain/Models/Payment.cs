﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WSDLLC.INoFear.Domain.Models
{
    [DataContract]
    public class Payment
    {
        [DataMember]
        public long PaymentId { get; set; }
        [DataMember]
        public string CreditCardNumber { get; set; }
        [DataMember]
        public string CardType { get; set; }
        [DataMember]
        public string SecurityCode { get; set; }
        [DataMember]
        public string ExperationYear { get; set; }
        [DataMember]
        public string ExperationMonth { get; set; }
        [DataMember]
        public string NameOnCard { get; set; }
        [DataMember]
        public bool IsInvalid { get; set; }
    }
}
