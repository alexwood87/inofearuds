﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WSDLLC.INoFear.Domain.Models
{
    [DataContract]
    public class Rating
    {
        [DataMember]
        public Protector Protector { get; set; }
        [DataMember]
        public Protectee Protectee { get; set; }
        [DataMember]
        public int RatingAmount { get; set; }
        [DataMember]
        public bool IsForProtector { get; set; }
    }
}
