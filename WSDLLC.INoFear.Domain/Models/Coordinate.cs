﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WSDLLC.INoFear.Domain.Models
{
    [DataContract]
    public class Coordinate
    {
        [DataMember]
        public Protectee Protectee { get; set; }
        [DataMember]
        public Protector Protector { get; set; }
        [DataMember]
        public decimal Latitude { get; set; }
        [DataMember]
        public decimal Longitude { get; set; }
        [DataMember]
        public DateTime DateCreated { get; set; }
        [DataMember]
        public long CoordinateId { get; set; }
    }
}
