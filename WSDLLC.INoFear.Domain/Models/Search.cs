﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WSDLLC.INoFear.Domain.Models
{
    [DataContract]
    public class Search
    {
        [DataMember]
        public long? Id { get; set; }
        [DataMember]
        public long? ProtectorId { get; set; }
        [DataMember]
        public long? ProtecteeId { get; set; }
        [DataMember]
        public int PageSize { get; set; }
        [DataMember]
        public int PageIndex { get; set; }
    }
}
