﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WSDLLC.INoFear.Domain.Models
{
    [DataContract]
    public class ResetCodeModel
    {
        [DataMember]
        public long ResetCodeId { get; set; }
        [DataMember]
        public string Code { get; set; }
        [DataMember]
        public DateTime DateCreated { get; set; }
        [DataMember]
        public string PhoneNumber { get; set; } 
    }
}
