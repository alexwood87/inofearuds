﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WSDLLC.INoFear.Domain.Models
{
    [DataContract]
    public class AdminstratorModel
    {
        [DataMember]
        public int AdminstratorId { get; set; }
        [DataMember]
        public string Username { get; set; }
        [DataMember]
        public string Pin { get; set; }
        [DataMember]
        public AuthenticationLevel AuthLevel { get; set; }

    }
}
