﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WSDLLC.INoFear.Domain.Models;
namespace WSDLLC.INoFear.Domain.Repository
{
    public interface ICoordinateRepository
    {
        List<Coordinate> SearchCoordinates(Search search);
        long AddUpdateCoordinates(Coordinate coordinate);
    }
}
