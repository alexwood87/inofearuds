﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WSDLLC.INoFear.Domain.Models;
namespace WSDLLC.INoFear.Domain.Repository
{
    public interface IAdminRepository
    {
        void AddOrUpdateAdmin(AdminstratorModel admin);
        List<AdminstratorModel> AllAdministrators();
        void CertifyProtector(long protectorId);
        void UnCertifyProtector(long protectorId);
        void ChangeProtectorType(long protectorId, string type);
        AdminstratorModel ValidateLogin(string username, string password);
        List<Protector> SearchProtectors(bool ceritifed, int pageIndex, out int total);
        void DeleteAdmin(int id);
        string IssueResetCode(string phoneNumber);
        void ResetPassword(string phoneNumber, string code, string pin);
    }
}
