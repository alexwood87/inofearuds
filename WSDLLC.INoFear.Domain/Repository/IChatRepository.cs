﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WSDLLC.INoFear.Domain.Models;

namespace WSDLLC.INoFear.Domain.Repository
{
    public interface IChatRepository
    {
        void AddChat(string chatMessage, DateTime createdDate, long protectorId, long protecteeId, bool isFromProtector);
        List<ChatModel> GetCurrentChats(long protecteeId, long protectorId, DateTime curTime, bool isfromProtector);
        void MarkChatSeen(Guid chatId);
    }
}
