﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WSDLLC.INoFear.Domain.Models;
namespace WSDLLC.INoFear.Domain.Repository
{
    public interface IPaymentTransactionRepository
    {
        long RegisterPayment(Payment p, string phoneNumber);
        long RegisterPaymentForProtector(long protectorId, Payment pay);
        long MakeTransaction(ProtectionTransaction p);
        List<Payment> SearchPayments(Search search);
        List<ProtectionTransaction> SearchTransactions(TransactionSearch search);
        void UpdatePayment(Payment p);
        bool DeletePayment(string phoneNumber, Payment p);
        void CancelTransaction(long transactionId);
    }
}
