﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WSDLLC.INoFear.Domain.Models;

namespace WSDLLC.INoFear.Domain.Repository
{
    public interface IProtectionRepository
    {
        long RegisterProtector(Protector p);
        long RegisterProtectee(Protectee p);
        List<Protectee> SearchProtectees(PersonDetailsSearch search);
        List<Protector> SearchProtectors(PersonDetailsSearch search);
        long Rate(Rating r);
        List<Rating> SearchRatings(Search search);
    }
}
