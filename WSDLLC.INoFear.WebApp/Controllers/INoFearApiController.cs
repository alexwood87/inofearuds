﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WSDLLC.INoFear.BusinessLogic;
using WSDLLC.INoFear.Core.Contracts;
using WSDLLC.INoFear.Domain.Models;
using System.Web.Script.Serialization;
namespace WSDLLC.INoFear.WebApp.Controllers
{
    public class INoFearApiController : ApiController
    {
        private readonly IAppBusinessLogic _logic;
        public INoFearApiController()
        {
            _logic = new AppBusinessLogic(ConfigurationManager.ConnectionStrings["ConnString"].ConnectionString);
        }
        
        [HttpGet]
        [HttpPost]
        public Protectee RegisterProtectee(Protectee p, Payment pay)
        {
            try
            {
                p.ProtecteeId = _logic.RegisterProtectee(p, pay);
                return
                    p;
            }
            catch (Exception ex)
            {
                return
                    new Protectee
                    {
                        ProtecteeId = -1
                    };
            }
        }

        [HttpGet]
        [HttpPost]
        public Protector RegisterOrUpdateProtector(Protector p, Payment pay)
        {
            try
            {
                p.ProtectorId = _logic.RegisterProtector(p, pay);
                return
                     p;
            }
            catch (Exception ex)
            {
                return
                    new Protector
                    {
                        ProtectorId = -1
                    };

            }
        }
        [HttpGet]
        [HttpPost]
        public List<Coordinate> SearchCoordinates(Search search)
        {
            return
                _logic.SearchCoordinates(search);
        }
        [HttpGet]
        [HttpPost]
        public Coordinate AddOrUpdateCoordinate(Coordinate coord, string payeePhoneNumber, string protectorPhoneNumber)
        {
            if (!string.IsNullOrEmpty(payeePhoneNumber))
            {
                coord.Protectee = _logic.SearchProtectees(new PersonDetailsSearch
                {
                    PhoneNumber = payeePhoneNumber,
                    PageSize = 1
                }).Single();
            }
            if (!string.IsNullOrEmpty(protectorPhoneNumber))
            {
                coord.Protector = _logic.SearchProtectors(new PersonDetailsSearch
                {
                    PageSize = 1,
                    PhoneNumber = protectorPhoneNumber
                }).Single();
            }
            coord.CoordinateId = _logic.AddUpdateCoordinates(coord);
            return coord;
        }
        [HttpGet]
        [HttpPost]
        public Protectee ValidateProtectee(string phoneNumber)
        {
            if (string.IsNullOrEmpty(phoneNumber))
            {
                return
                    new Protectee
                    {
                        ProtecteeId = -1
                    };
            }
            var p = _logic.SearchProtectees(new PersonDetailsSearch
            {
                PhoneNumber = phoneNumber,
                PageSize = 1
            }).SingleOrDefault();
            return
                p == null ? new Protectee
                {
                    ProtecteeId = -1
                } : p;
        }
        [HttpGet]
        [HttpPost]
        public Protector ValidatePrortector(string phoneNumber)
        {
            if (string.IsNullOrEmpty(phoneNumber))
            {
                return
                    new Protector
                    {
                        ProtectorId = -1
                    };
            }
            var p = _logic.SearchProtectors(new PersonDetailsSearch
            {
                PageSize = 1,
                PhoneNumber = phoneNumber
            }).SingleOrDefault();
            return
                p == null ? new Protector
                {
                    ProtectorId = -1,
                    IsCertified = false
                } : p;
        }
        [HttpGet]
        [HttpPost]
        public List<Protector> SearchProtectors(PersonDetailsSearch search)
        {
            return
                _logic.SearchProtectors(search);
        }
        [HttpGet]
        [HttpPost]
        public List<Protectee> SearchProtectees(PersonDetailsSearch search)
        {
            return
                _logic.SearchProtectees(search);
        }
        [HttpGet]
        [HttpPost]
        public List<ProtectionTransaction> SearchTransactions(TransactionSearch search)
        {
            return
                _logic.SearchTransactions(search);
        }
        [HttpGet]
        [HttpPost]
        public List<Payment> SearchPayments(string payeePhoneNumber)
        {
            var payee = _logic.SearchProtectees(new PersonDetailsSearch
            {
                PhoneNumber = payeePhoneNumber,
                PageSize = 1
            }).SingleOrDefault();
            if (payee == null)
            {
                return
                    new List<Payment>
                    {

                    };
            }
            return _logic.SearchPayments(new Search
            {
                ProtecteeId = payee.ProtecteeId,
                PageSize = int.MaxValue
            });
        }
        [HttpGet]
        [HttpPost]
        public Payment RegisterPayment(Payment p, string phoneNumber)
        {
            try
            {
                var payId = _logic.RegisterPayment(p, phoneNumber);
                p.PaymentId = payId;

                return p == null
                    || p.IsInvalid ?
                    new Payment
                    {
                        IsInvalid = false,
                        PaymentId = -1
                    } : p;

            }
            catch (Exception ex)
            {
                return
                    new Payment
                    {
                        PaymentId = -1,
                        IsInvalid = false
                    };
            }
        }
        [HttpGet]
        [HttpPost]
        public ProtectionTransaction MakeTranaction(DateTime startTime, string protectorPhoneNumber, string protecteePhoneNumber, decimal amount, Payment p, decimal
            startLatitude, decimal startLongitude, decimal endLatitude, decimal endLongitude)
        {
            try
            {
                var protectee = _logic.SearchProtectees(new PersonDetailsSearch
                {
                    PhoneNumber = protecteePhoneNumber,
                    PageSize = 1
                }).Single();
                var pro = _logic.SearchProtectors(new PersonDetailsSearch
                {
                    PageSize = 1,
                    PhoneNumber = protectorPhoneNumber
                }).Single();
                var c1Id = _logic.AddUpdateCoordinates(new Coordinate
                {
                    DateCreated
                     = startTime,
                    Latitude = startLatitude,
                    Longitude = startLongitude,
                    Protectee = protectee,
                    Protector = pro
                });
                var c2Id = _logic.AddUpdateCoordinates(new Coordinate
                {
                    DateCreated = DateTime.UtcNow,
                    Protector = pro,
                    Protectee = protectee,
                    Latitude = endLatitude,
                    Longitude = endLongitude
                });
                var trans = _logic.MakeTransaction(new ProtectionTransaction
                {
                    AmountCharged = amount,
                    DateCreated = DateTime.UtcNow,
                    EndCoordinate = _logic.SearchCoordinates(new Search
                    {
                        Id = c2Id,
                        PageSize = 1
                    }).Single(),
                    EndDate = DateTime.UtcNow,
                    StartCoordinate = _logic.SearchCoordinates(new Search
                    {
                        Id = c1Id,
                        PageSize = 1
                    }).Single(),
                    Protectee = protectee,
                    Protector = pro,
                    WasCancelled = false
                });
                return
                    _logic.SearchTransactions(new TransactionSearch
                    {
                        Id = trans,
                        PageSize = 1
                    }).Single();
            }
            catch (Exception ex)
            {
                return
                    new ProtectionTransaction
                    {
                        ProtectionTransactionId = -1
                    };
            }
        }
    }
}
