﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WSDLLC.INoFear.BusinessLogic;
using WSDLLC.INoFear.Core.Contracts;
using WSDLLC.INoFear.Domain.Models;

namespace WSDLLC.INoFear.WebApp.Controllers
{
    public class INoFearController : Controller
    {
        private readonly IAppBusinessLogic _logic;
        public INoFearController()
        {
            _logic = new AppBusinessLogic(ConfigurationManager.ConnectionStrings["ConnString"].ConnectionString);
        }
        [HttpGet]
        [HttpPost]
        public JsonResult RegisterProtectee(Protectee p, Payment pay)
        {
            try
            {
                p.ProtecteeId = _logic.RegisterProtectee(p, pay);
                return
                    Json(new { Success = true, Data = p }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new {
                    Data = new Protectee
                    {
                        ProtecteeId = -1
                    }, Success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [HttpPost]
        public JsonResult RegisterOrUpdateProtector(Protector p, Payment pay)
        {
            try
            {
                p.ProtectorId = _logic.RegisterProtector(p, pay);
                return
                     Json(new { Data = p, Success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json( new
                {
                    Success = false,
                    Data =
                    new Protector
                    {
                        ProtectorId = -1
                    }
                }, JsonRequestBehavior.AllowGet);

            }
        }
        [HttpGet]
        [HttpPost]
        public JsonResult SearchCoordinates(Search search)
        {
            try
            {
                return Json(new
                {
                    Success = true,
                    Data = _logic.SearchCoordinates(search)
                }, JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                return
                    Json(new { Success = false, Error = ex.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        [HttpPost]
        public JsonResult AddOrUpdateCoordinate(Coordinate coord, string payeePhoneNumber, string protectorPhoneNumber)
        {
            try
            {
                if (!string.IsNullOrEmpty(payeePhoneNumber))
                {
                    coord.Protectee = _logic.SearchProtectees(new PersonDetailsSearch
                    {
                        PhoneNumber = payeePhoneNumber,
                        PageSize = 1
                    }).Single();
                }
                if (!string.IsNullOrEmpty(protectorPhoneNumber))
                {
                    coord.Protector = _logic.SearchProtectors(new PersonDetailsSearch
                    {
                        PageSize = 1,
                        PhoneNumber = protectorPhoneNumber
                    }).Single();
                }
                coord.CoordinateId = _logic.AddUpdateCoordinates(coord);
                return Json(new { Data = coord, Success = true }, JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                return
                    Json(new { Success = false, Error = ex.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        [HttpPost]
        public JsonResult ValidateProtectee(string phoneNumber)
        {
            try
            {
                if (string.IsNullOrEmpty(phoneNumber))
                {
                    return
                        Json(new { Success = false, Error = "Phone Number is null"}, JsonRequestBehavior.AllowGet);
                }
                var p = _logic.SearchProtectees(new PersonDetailsSearch
                {
                    PhoneNumber = phoneNumber,
                    PageSize = 1
                }).SingleOrDefault();
                return
                    p == null ? Json(new { Success = false}, JsonRequestBehavior.AllowGet) : Json(new { Data = p, Success = true }, JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                return
                    Json(new { Error = ex.ToString(), Success = false }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        [HttpPost]
        public JsonResult ValidatePrortector(string phoneNumber)
        {
            try {
                if (string.IsNullOrEmpty(phoneNumber))
                {
                    return
                        Json(new {
                            Success = false,
                            Error = "Phone number is null"
                        }, JsonRequestBehavior.AllowGet);
                }
                var p = _logic.SearchProtectors(new PersonDetailsSearch
                {
                    PageSize = 1,
                    PhoneNumber = phoneNumber
                }).SingleOrDefault();
                return
                    p == null ? Json(new { Success = false }, JsonRequestBehavior.AllowGet) : Json(new { Data = p, Success = true }, JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                return
                    Json(new { Error = ex.ToString(), Success = false}, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        [HttpPost]
        public JsonResult SearchProtectors(PersonDetailsSearch search)
        {
            try { 
                return Json(new
                    {
                        Data =
                        _logic.SearchProtectors(search),
                        Success = true
                    }, JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                return
                    Json(new { Error = ex.ToString(), Success = false }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        [HttpPost]
        public JsonResult SearchProtectees(PersonDetailsSearch search)
        {
            try
            {
                return Json(new
                {
                    Data =
                    _logic.SearchProtectees(search),
                    Success = true
                }, JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                return
                    Json(new { Error = ex.ToString(), Success = false }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        [HttpPost]
        public JsonResult SearchTransactions(TransactionSearch search)
        {
            try
            {
                return Json(new
                {
                    Success = true,
                    Data =
                    _logic.SearchTransactions(search)
                }, JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                return
                    Json(new {Error = ex.ToString(), Success = false }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        [HttpPost]
        public JsonResult SearchPayments(string payeePhoneNumber)
        {
            try
            {
                var payee = _logic.SearchProtectees(new PersonDetailsSearch
                {
                    PhoneNumber = payeePhoneNumber,
                    PageSize = 1
                }).SingleOrDefault();
                if (payee == null)
                {
                    return
                        Json(new { Success = false, Error ="Phone Number Not Found" }, JsonRequestBehavior.AllowGet);
                }
                return Json(new { Data = _logic.SearchPayments(new Search
                {
                    ProtecteeId = payee.ProtecteeId,
                    PageSize = int.MaxValue
                }), Success = true}, JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                return
                    Json(new { Error = ex.ToString(), Success = false }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        [HttpPost]
        public JsonResult RegisterPayment(Payment p, string phoneNumber)
        {
            try
            {
                var payId = _logic.RegisterPayment(p, phoneNumber);
                p.PaymentId = payId;

                return p == null
                    || p.IsInvalid ?
                    Json(new { Success = false, Error = "Failed To Register Payment" }, JsonRequestBehavior.AllowGet) 
                    : Json(new { Data =p, Success = true }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return
                    Json(new { Success = false, Error = ex.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        [HttpPost]
        public JsonResult MakeTranaction(DateTime startTime, string protectorPhoneNumber, string protecteePhoneNumber, decimal amount, Payment p, decimal
            startLatitude, decimal startLongitude, decimal endLatitude, decimal endLongitude)
        {
            try
            {
                var protectee = _logic.SearchProtectees(new PersonDetailsSearch
                {
                    PhoneNumber = protecteePhoneNumber,
                    PageSize = 1
                }).Single();
                var pro = _logic.SearchProtectors(new PersonDetailsSearch
                {
                    PageSize = 1,
                    PhoneNumber = protectorPhoneNumber
                }).Single();
                var c1Id = _logic.AddUpdateCoordinates(new Coordinate
                {
                    DateCreated
                     = startTime,
                    Latitude = startLatitude,
                    Longitude = startLongitude,
                    Protectee = protectee,
                    Protector = pro
                });
                var c2Id = _logic.AddUpdateCoordinates(new Coordinate
                {
                    DateCreated = DateTime.UtcNow,
                    Protector = pro,
                    Protectee = protectee,
                    Latitude = endLatitude,
                    Longitude = endLongitude
                });
                var trans = _logic.MakeTransaction(new ProtectionTransaction
                {
                    AmountCharged = amount,
                    DateCreated = DateTime.UtcNow,
                    EndCoordinate = _logic.SearchCoordinates(new Search
                    {
                        Id = c2Id,
                        PageSize = 1
                    }).Single(),
                    EndDate = DateTime.UtcNow,
                    StartCoordinate = _logic.SearchCoordinates(new Search
                    {
                        Id = c1Id,
                        PageSize = 1
                    }).Single(),
                    Protectee = protectee,
                    Protector = pro,
                    WasCancelled = false
                });
                return Json(new
                {
                    Data =
                    _logic.SearchTransactions(new TransactionSearch
                    {
                        Id = trans,
                        PageSize = 1
                    }).Single(),
                    Success = true
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return
                    Json(new { Error = ex.ToString(), Success = false }, JsonRequestBehavior.AllowGet);
            }
        }

    }
}