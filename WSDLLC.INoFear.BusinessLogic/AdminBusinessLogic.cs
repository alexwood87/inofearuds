﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WSDLLC.INoFear.BusinessLogic.Helpers;
using WSDLLC.INoFear.Core.Contracts;
using WSDLLC.INoFear.Domain.Models;
using WSDLLC.INoFear.Domain.UnitOfWork;
using WSDLLC.INoFear.Infrastructure.UnitOfWork;

namespace WSDLLC.INoFear.BusinessLogic
{
    public class AdminBusinessLogic : IAdminBusinessLogic
    {
        private readonly IUnitOfWork _unitOfOWork;
        public AdminBusinessLogic(string connString)
        {
            _unitOfOWork = new UnitOfWork(connString);
        }
        public void AddOrUpdateAdmin(AdminstratorModel admin, AuthenticationLevel level)
        {
            if(level != AuthenticationLevel.SUPERADMIN)
            {
                throw new UnauthorizedAccessException("You do not have access to modify an admin!");
            }
            _unitOfOWork.AdminRepository.AddOrUpdateAdmin(admin);
        }

        public List<AdminstratorModel> AllAdministrators(AuthenticationLevel level)
        {
            if(level != AuthenticationLevel.SUPERADMIN)
            {
                return null;
            }
            return
                _unitOfOWork.AdminRepository.AllAdministrators();
        }

        public void CertifyProtector(long protectorId)
        {
            _unitOfOWork.AdminRepository.CertifyProtector(protectorId);
        }

        public void ChangeProtectorType(long protectorId, string type)
        {
            _unitOfOWork.AdminRepository.ChangeProtectorType(protectorId, type);
        }

        public void SendStatusEmail(long? protectorId, long? protecteeId, string message, string title)
        {
            string email = null;
            if(protectorId  != null)
            {
                email = _unitOfOWork.ProtectionRepository.SearchProtectors(new PersonDetailsSearch
                {
                    ProtectorId = protectorId.Value,
                    PageSize = 1
                }).Single().Email;
            }
            if(protecteeId != null)
            {
                email = _unitOfOWork.ProtectionRepository.SearchProtectees(new PersonDetailsSearch
                {
                    PageSize = 1,
                    ProtecteeId = protecteeId.Value
                }).Single().Email;
            }
            if(email != null)
            {
                App.Common.CommonFunctions.SendEmail(ConfigurationManager.AppSettings["InfoFromEmail"], email, message, title,
                ConfigurationManager.AppSettings["EmailUsername"], ConfigurationManager.AppSettings["EmailPassword"]);
            }
        }
        public void DeleteAdmin(int id, AuthenticationLevel level)
        {
            if(level != AuthenticationLevel.SUPERADMIN)
            {
                throw new UnauthorizedAccessException("You do not have authority to modify a user");
            }
            _unitOfOWork.AdminRepository.DeleteAdmin(id);
        }
        public List<Protector> SearchProtectors(bool ceritifed, int pageIndex, out int total)
        {
            return
                _unitOfOWork.AdminRepository.SearchProtectors(ceritifed, pageIndex, out total);
        }
        public void UnCertifyProtector(long protectorId)
        {
            _unitOfOWork.AdminRepository.UnCertifyProtector(protectorId);
        }

        public AdminstratorModel ValidateLogin(string username, string password)
        {
            return
                _unitOfOWork.AdminRepository.ValidateLogin(username, password);
        }

        public void IssueResetCode(string phoneNumber)
        {
            phoneNumber = InputHelper.CleanPhone(phoneNumber);
 
           string code = _unitOfOWork.AdminRepository.IssueResetCode(phoneNumber);
            var res = App.Common.CommonFunctions.SendSMS(ConfigurationManager.AppSettings["SMSAcountID"],
                ConfigurationManager.AppSettings["SMSEmail"], ConfigurationManager.AppSettings["SMSPassword"],
                phoneNumber, "Your Reset Code Is: " + code, ConfigurationManager.AppSettings["SMSProvider"]);
            if(res != 0)
            {
                var pro = _unitOfOWork.ProtectionRepository.SearchProtectees(new PersonDetailsSearch
                {
                    PageSize = 1,
                    PhoneNumber = phoneNumber
                }).SingleOrDefault();
                if (pro != null)
                {
                    App.Common.CommonFunctions.SendEmail(ConfigurationManager.AppSettings["FromEmail"],
                           pro.Username, pro.FirstName + " " + pro.LastName + " your code is: " + code, "UBS Password Reset", ConfigurationManager.AppSettings["EmailUsername"],
                           ConfigurationManager.AppSettings["EmailPassword"]);
                }
                var pre = _unitOfOWork.ProtectionRepository.SearchProtectors(new PersonDetailsSearch
                {
                    PageSize = 1,
                    PhoneNumber = phoneNumber
                }).SingleOrDefault();
                if(pre != null)
                {
                    App.Common.CommonFunctions.SendEmail(ConfigurationManager.AppSettings["FromEmail"],
                          pre.Username, pre.FirstName + " " + pre.LastName +  " your code is: " + code, "UBS Password Reset", ConfigurationManager.AppSettings["EmailUsername"],
                          ConfigurationManager.AppSettings["EmailPassword"]);
                }
            }
        }

        public void ResetPassword(string phoneNumber, string code, string pin)
        {
            phoneNumber = InputHelper.CleanPhone(phoneNumber);
            _unitOfOWork.AdminRepository.ResetPassword(phoneNumber, code, pin);
        }
    }
}
