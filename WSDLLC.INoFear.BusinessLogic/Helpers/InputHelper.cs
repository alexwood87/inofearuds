﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WSDLLC.INoFear.BusinessLogic.Helpers
{
    public class InputHelper
    {
        public static string CleanNumbers(string numberString)
        {
            if (numberString == null)
            {
                return null;
            }
            var sb = new StringBuilder();
            foreach (var p in numberString.ToCharArray())
            {
                switch (p)
                {
                    case '0':
                    case '1':
                    case '2':
                    case '3':
                    case '4':
                    case '5':
                    case '6':
                    case '7':
                    case '8':
                    case '9':
                        sb.Append(p);
                        break;
                }
            }
            return sb.ToString();
        }

        public static string CleanPhone(string phone)
        {
            if (phone == null)
            {
                return null;
            }
            var sb = new StringBuilder();
            foreach (var p in phone.ToCharArray())
            {
                switch (p)
                {
                    case '0':
                    case '1':
                    case '2':
                    case '3':
                    case '4':
                    case '5':
                    case '6':
                    case '7':
                    case '8':
                    case '9':
                    case '+':
                        sb.Append(p);
                        break;
                    default:
                        continue;
                }
            }
            return sb.ToString();
        }
    }
}
