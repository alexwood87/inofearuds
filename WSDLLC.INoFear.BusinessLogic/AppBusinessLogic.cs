﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using WSDLLC.INoFear.BusinessLogic.Helpers;
using WSDLLC.INoFear.Core.Contracts;
using WSDLLC.INoFear.Domain.Models;
using WSDLLC.INoFear.Domain.UnitOfWork;
using WSDLLC.INoFear.Infrastructure.UnitOfWork;

namespace WSDLLC.INoFear.BusinessLogic
{
    public class AppBusinessLogic : IAppBusinessLogic
    {
        private readonly IUnitOfWork _unitOfWork;
        public AppBusinessLogic(string connString)
        {
            _unitOfWork = new UnitOfWork(connString);
        }
        public long AddUpdateCoordinates(Coordinate coordinate)
        {
            if (coordinate.DateCreated == DateTime.MinValue)
            {
                coordinate.DateCreated = DateTime.UtcNow;
            }
            return _unitOfWork.CoordinateRepository.AddUpdateCoordinates(coordinate);
        }
        public void CancelTransaction(long transactionId)
        {
            _unitOfWork.PaymentTransactionRepository.CancelTransaction(transactionId);
            var trans = _unitOfWork.PaymentTransactionRepository.SearchTransactions(new TransactionSearch
            {
                Id = transactionId,
                PageSize = 1
            }).Single();
            var otherTrans = _unitOfWork.PaymentTransactionRepository.SearchTransactions(new TransactionSearch
            {
                PageSize = int.MaxValue,
                ProtecteePhoneNumber = trans.Protectee.PhoneNumber,
                WasCancelled = false
            });
            foreach(var t in otherTrans)
            {
                _unitOfWork.PaymentTransactionRepository.CancelTransaction(t.ProtectionTransactionId);
            }
        }
        public bool DeletePayment(string phoneNumber, Payment p)
        {
            phoneNumber = InputHelper.CleanPhone(phoneNumber);
         
            p.CreditCardNumber = InputHelper.CleanNumbers(p.CreditCardNumber);
            p.ExperationMonth = InputHelper.CleanNumbers(p.ExperationMonth.Substring(0, 2));
            p.ExperationYear = InputHelper.CleanNumbers(p.ExperationYear.Substring(0, 2));
            p.SecurityCode = InputHelper.CleanNumbers(p.SecurityCode);
            p.IsInvalid = true;
            return _unitOfWork.PaymentTransactionRepository.DeletePayment(phoneNumber, p);
        }
        public long MakeTransaction(ProtectionTransaction p)
        {
            var payment = _unitOfWork.PaymentTransactionRepository.SearchPayments(new Search
            {
                PageSize = 1,
                Id = p.PaymentId
            }).Single();
            var service = new MerchantAccountService.AccountService();
            int result = -1;
            if (p.Protector != null)
            {
                if (p.AmountCharged <= 0)
                {
                    result = service.RunAcceptPayment(payment);
                    p.AmountCharged = (decimal)100.00;
                }
                else
                {
                     p.AmountCharged = service.RunPayment(payment, p.DateCreated, p.EndDate, p.StartCoordinate, p.EndCoordinate, out result);
                }
            }
            else
            {
                result = 0;
            }
            if (result == 0)
            {
                return _unitOfWork.PaymentTransactionRepository.MakeTransaction(p);
            }
            else
            {
                payment.IsInvalid = true;
                _unitOfWork.PaymentTransactionRepository.UpdatePayment(payment);
                return -1;
            }
        }

        public long Rate(Rating r)
        {
            return _unitOfWork.ProtectionRepository.Rate(r);
        }
        private Payment CleanPayment(Payment p)
        {
            if(p == null)
            {
                return null;
            }
            p.SecurityCode = InputHelper.CleanNumbers(p.SecurityCode);
            p.ExperationYear = InputHelper.CleanNumbers(p.ExperationYear);
            p.ExperationMonth = InputHelper.CleanNumbers(p.ExperationMonth);
            p.CreditCardNumber = InputHelper.CleanNumbers(p.CreditCardNumber);
            return
                p;
        }
        public long RegisterPayment(Payment p, string phoneNumber)
        {
            phoneNumber = InputHelper.CleanPhone(phoneNumber);
            p = CleanPayment(p);
            var dt = DateTime.UtcNow;
            if (dt.Year - 2000 > int.Parse(p.ExperationYear))
            {
                throw new ArgumentException("Experation Year is invalid");
            }
            int month;
            if (!int.TryParse(p.ExperationMonth, out month) || month < 1 || month > 12)
            {
                throw new ArgumentException("Experation Month Is Invalid");
            }
            if (string.IsNullOrEmpty(phoneNumber))
            {
                throw new ArgumentException("Phone Number Is Invalid");
            }
            long l;
            if (string.IsNullOrEmpty(p.CreditCardNumber)|| !long.TryParse(p.CreditCardNumber, out l))
            {
                throw new ArgumentException("Credit Card Number Is Invalid");
            }
            if(string.IsNullOrEmpty(p.NameOnCard))
            {
                throw new ArgumentException("Name On Card Is Invalid");
            }
            if(!int.TryParse(p.SecurityCode, out month))
            {
                throw new ArgumentException("Security Code Is Invalid");
            }
            var accountService = new MerchantAccountService.AccountService();
            if(!accountService.ValidatePayment(p))
            {
                throw new ArgumentException("Payment Is Invalid");
            }
            return _unitOfWork.PaymentTransactionRepository.RegisterPayment(p, phoneNumber);
        }
        private bool IsValidEmail(string email)
        {
            try
            {
                new MailAddress(email);
                return true;
            }
            catch(Exception ex)
            {
                return
                    false;
            }
        }
        
        public long RegisterProtectee(Protectee p, Payment pay)
        {
            pay = CleanPayment(pay);
            p.PhoneNumber = InputHelper.CleanPhone(p.PhoneNumber);
            if(!IsValidEmail(p.Email))
            {
                throw new ArgumentException("Email Is Invalid");
            }
            var id = _unitOfWork.ProtectionRepository.RegisterProtectee(p);

            return pay == null ? id : _unitOfWork.PaymentTransactionRepository.RegisterPayment(pay, p.PhoneNumber);

        }
       
        public long RegisterProtector(Protector p, Payment pay)
        {
            
            p.PhoneNumber = InputHelper.CleanPhone(p.PhoneNumber);
            
            if (!IsValidEmail(p.Email))
            {
                throw new ArgumentException("Email Is Invalid");
            }
            var l = _unitOfWork.ProtectionRepository.RegisterProtector(p);
            pay = CleanPayment(pay);
            if(pay != null)
            _unitOfWork.PaymentTransactionRepository.RegisterPaymentForProtector(l, pay);
            return
                l;
        }

        public List<Coordinate> SearchCoordinates(Search search)
        {
            return
                _unitOfWork.CoordinateRepository.SearchCoordinates(search);
        }

        public List<Payment> SearchPayments(Search search)
        {
            return
                _unitOfWork.PaymentTransactionRepository.SearchPayments(search);
        }

        public List<Protectee> SearchProtectees(PersonDetailsSearch search)
        {
            return
                _unitOfWork.ProtectionRepository.SearchProtectees(search);
        }
        
        public List<Protector> SearchProtectors(PersonDetailsSearch search)
        {
            return
                _unitOfWork.ProtectionRepository.SearchProtectors(search);
        }

        public List<Rating> SearchRatings(string protectorPhone, long? ratingId)
        {
            return
                _unitOfWork.ProtectionRepository.SearchRatings(new Search
                {
                    PageSize = int.MaxValue,
                    Id = ratingId,
                    ProtectorId = _unitOfWork.ProtectionRepository.SearchProtectors(new PersonDetailsSearch
                    {
                        PhoneNumber = protectorPhone
                    }).OrderByDescending(x => x.ProtectorId).First().ProtectorId
                }).ToList();
        }

        public List<ProtectionTransaction> SearchTransactions(TransactionSearch search)
        {
            return
                _unitOfWork.PaymentTransactionRepository.SearchTransactions(search);
        }

        public ChatModel GetMostCurrentChat(string protectorPhone, string protecteePhone, bool isFromProtector)
        {
            var proId = _unitOfWork.ProtectionRepository.SearchProtectors(new PersonDetailsSearch
            {
                PhoneNumber = protectorPhone,
                PageSize = 1,
                PageIndex = 0
            }).Single().ProtectorId;
            var proteeId = _unitOfWork.ProtectionRepository.SearchProtectees(new PersonDetailsSearch
            {
                PageSize = 1,
                PageIndex = 0,
                PhoneNumber = protecteePhone
            }).Single().ProtecteeId;
            var chat =
                _unitOfWork.ChatRepository.GetCurrentChats(proteeId, proId, DateTime.UtcNow.AddHours(-6).AddMinutes(-5), isFromProtector)
                    .OrderByDescending(c => c.CreatedDate).FirstOrDefault();
            if(chat != null)
            {
                _unitOfWork.ChatRepository.MarkChatSeen(chat.ChatId);
            }
            return chat;
        }

        public void AddChat(string message, string protectorPhone, string protecteePhone, bool isFromProtector)
        {
            var proId = _unitOfWork.ProtectionRepository.SearchProtectors(new PersonDetailsSearch
            {
                PhoneNumber = protectorPhone,
                PageSize = 1,
                PageIndex = 0
            }).Single().ProtectorId;
            var proteeId = _unitOfWork.ProtectionRepository.SearchProtectees(new PersonDetailsSearch
            {
                PageSize = 1,
                PageIndex = 0,
                PhoneNumber = protecteePhone
            }).Single().ProtecteeId;
            _unitOfWork.ChatRepository.AddChat(message, DateTime.UtcNow.AddHours(-6), proId, proteeId, isFromProtector);
        }
    }
}
