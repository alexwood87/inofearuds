CREATE TABLE Protectors(ProtectorId bigint primary key identity(1,1),
FirstName nvarchar(100) not null, LastName NVArchar(100), 
Picture Image not null, Username nvarchar(100) not null,
[Password] NVarchar(100) not null, PhoneNumber Nvarchar(100) Not Null,
IsCertified bit not null default(0),ProtectionType NVarchar(100) not null,
)

CREATE TABLE Protectees(ProtecteeId bigint primary key identity(1,1),
Username nvarchar(100) not null, PhoneNumber nvarchar(100) not null,
FirstName nvarchar(100) not null, LastName nvarchar(100) not null,
[Password] nvarchar(100) not null, Picture Image not null)

CREATE TABLE Payments(PaymentId bigint primary key identity(1,1),
CreditCardNumber nvarchar(100), SecurityCode nvarchar(100) not null, 
ExperationYear nvarchar(10) not null, ExperationMonth nvarchar(10) not null,
NameOnCard NVArchar(100) not null, IsInvalid bit not null default(0),
 CardType nvarchar(100) not null 
)

CREATE TABLE PaymentToProtector(PaymentToProtectorId bigint primary key identity(1,1), 
ProtectorId bigint not null, PaymentId bigint not null,
foreign key(ProtectorId) references Protectors(ProtectorId),
foreign key(PaymentId) references Payments(PaymentId))
CREATE TABLE PaymentToProtectee(PaymentToProtecteeId bigint identity(1,1) primary key,
PaymentId bigint not null, ProtecteeId bigint not null, ProtectionType NVarchar(100) not null,
foreign key(PaymentId) references Payments(PaymentId),
foreign key(ProtecteeId) references Protectees(ProtecteeId))

CREATE TABLE Coordinates(CoordinateId bigint primary key identity(1,1),
Latitude decimal(10,10) not null, Longitude decimal(10,10) not null,
ProtecteeId bigint  null, 
ProtectorId bigint  null,
DateCreated datetime not null default(GETDATE()),
foreign key(ProtectorId) references Protectors(ProtectorId),
foreign key(ProtecteeId) references Protectees(ProtecteeId))

CREATE TABLE ProtectionTransaction(ProtectionTransactionId bigint primary key identity(1,1),
ProtectorId bigint null, ProtecteeId bigint not null, 
DateCreated DateTime not null default(GetDate()),
AmountCharged decimal(20,2) not null default(0), 
PaymentId bigint not null,
WasCancelled bit not null default(0),
EndCoordinateId bigint null, StartCoordinateId bigint not null,
EndDate DateTime null,
foreign key(PaymentId) references Payments(PaymentId),
foreign key(StartCoordinateId) references Coordinates(CoordinateId), 
foreign key(EndCoordinateId) references Coordinates(CoordinateId),
foreign key(ProtectorId) references Protectors(ProtectorId),
foreign key(ProtecteeId) references Protectees(ProtecteeId))


CREATE TABLE Rating(RatingId bigint primary key identity(1,1),
ProtectorId bigint not null, Rating int not null, ProtecteeId bigint not null,
foreign key(ProtecteeId) references Protectees(ProtecteeId),
foreign key(ProtectorId) references Protectors(ProtectorId))

ALTER TABLE Payments
ADD CardType nvarchar(100) not null 